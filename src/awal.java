
import gudang.batch;
import gudang.estimasiNilai;
import gudang.persediaan;
import gudang.riwayat;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import keuangan.bayarFakturPembelian;
import keuangan.bayarFakturPenjualan;
import keuangan.fakturPembelian;
import keuangan.fakturPenjualan;
import master.barang;
import master.pelanggan;
import master.salesman;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import pajak.exportFaktur;
import pembelian.PurchaseOrder;
import pembelian.orderPembelian;
import pembelian.terimaBarang;
import penjualan.SalesOrder;
import penjualan.cekGudang;
import penjualan.cetakFaktur;
import penjualan.laporanPenjualan;
import penjualan.laporanPiutang;
import utility.getConnection;
import utility.penangananKomponen;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author iweks24
 */
public class awal extends javax.swing.JFrame {

    Connection kon;
    String user = "";
    getConnection u;
    penangananKomponen kom;
    String folder;
    pengaturan p_pengaturan;
    Image image = null;
    cetakFaktur cetakFaktur;
    exportFaktur exportFaktur;
    SalesOrder SalesOrder;
    laporanPiutang laporanPiutang;
    laporanPenjualan laporanPenjualan;
    cekGudang cekGudang;
    persediaan persediaan;
    estimasiNilai estimasiNilai;
    riwayat riwayat;
    batch batch;
    orderPembelian orderPembelian;
    terimaBarang terimaBarang;
    fakturPembelian fakturPembelian;
    fakturPenjualan fakturPenjualan;
    PurchaseOrder PurchaseOrder;
    bayarFakturPenjualan bayarFakturPenjualan;
    bayarFakturPembelian bayarFakturPembelian;
    barang barang;
    pelanggan pelanggan;
    salesman salesman;

    /**
     * Creates new form awal
     */
    public awal() {
        u = new getConnection();
        kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        kom = new penangananKomponen();

        if (kon == null) {
            setVisible(false);
            new pengaturan();
        } else {
            folder = System.getProperty("user.dir");
//            try {
//                image = ImageIO.read(new File(folder + "\\image\\bg.png"));
//            } catch (IOException ex) {
//                //   Logger.getLogger(jendelaUtama.class.getName()).log(Level.SEVERE, null, ex);
//            }
            initComponents();
            java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screen);
            //    toolbar.setVisible(false);
            //    menuBar.setVisible(false);
//            final DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
//            final DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
//            ActionListener taskPerformer = new ActionListener() {
//                public void actionPerformed(ActionEvent evt) {
//
//                    java.util.Date date = new java.util.Date();
//
//                    String datestring = dateFormat.format(date);
//                    String datestring2 = dateFormat2.format(date);
//                    jam.setText(datestring);
//                    jam1.setText(datestring2);
//
//                }
//            };
//            new Timer(1000, taskPerformer).start();

            try {
                this.setIconImage(ImageIO.read(new File(folder + "\\image\\icon.jpg")));
            } catch (IOException ex) {
                //     Logger.getLogger(awal.class.getName()).log(Level.SEVERE, null, ex);
            }
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    tutup();
                }
            });
            setLocation(0, 0);
            setVisible(true);

            tampil();
        }
    }

    // login 
    private static int login(String url, String db, String login, String password) throws XmlRpcException, MalformedURLException {
        XmlRpcClient client = new XmlRpcClient();
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setEnabledForExtensions(true);
        //config.setServerURL(new URL(url+"/xmlrpc/common"));
        config.setServerURL(new URL(url + "/xmlrpc/2/common"));
        client.setConfig(config);
        //Connect
        //Object[] empty = null; // Ok
        //Object[] params = new Object[] {db,login,password, empty}; // Ok
        Object[] params = new Object[]{db, login, password}; // Ok & simple
        Object uid = client.execute("login", params);
        if (uid instanceof Integer) {
            return (int) uid;
        }
        return -1;
    }

    private void tutup() {
        int yyy = JOptionPane.showConfirmDialog(this, "Anda Yakin ingin menutup Aplikasi?", "Konfirmasi", JOptionPane.YES_NO_OPTION);
        if (yyy == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    private void tampil() {
        java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int wt = 380;
        int ht = 170;
        int w = screen.width;
        int h = screen.height;
        login.setLocation((w - wt) / 2, ((h - ht) / 2) - (ht / 2));
        login.setSize(wt, ht);
        d_user.requestFocus();
        login.setVisible(true);
    }

    public void showFrame(JInternalFrame jss, String judul) {

        boolean frame = this.isLoaded(judul);
        Dimension d = tengah.getSize();
        if (!frame) {
            try {
                //     slideMenu.add(addPanel(judul, judul, jss));

                jss.setSize(d);

                this.tengah.add(jss);
                jss.setMaximum(true);
                jss.show();
                jss.setSelected(true);

            } catch (java.beans.PropertyVetoException e) {
            }
        }
    }

    public boolean isLoaded(String FormTitle) {
        javax.swing.JInternalFrame Form[] = this.tengah.getAllFrames();
        for (int i = 0; i < Form.length; i++) {
            if (Form[i].getTitle().equalsIgnoreCase(FormTitle)) {
                Form[i].setLocation(0, 0);

                Form[i].show();

                try {
                    Form[i].setIcon(false);
                    Form[i].setSelected(true);
                } catch (java.beans.PropertyVetoException e) {
                }
                return true;
            }
        }
        return false;
    }

    private boolean login(String user, String pass) {
        boolean hasil = false;
        int a = 0;
        try {
            a = login("http://" + u.ip, u.db, user, pass);
        } catch (XmlRpcException ex) {
            Logger.getLogger(awal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(awal.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (a > 0) {
            hasil = true;
        }

        return hasil;
    }

    private void suksesLogin() {
        user = this.d_user.getText();

        d_user.setText("");
        d_pass.setText("");
    }

    private void goLogin() {
        String a = d_user.getText().toLowerCase();
        String b = d_pass.getText().toLowerCase();
        boolean ok = login(a, b);
        if (ok) {
            suksesLogin();
            login.dispose();
//            boolean yes = cekAkses(a, "CSO");
//            if (yes) {
//                suksesLogin();
//                login.dispose();
//            } else {
//                JOptionPane.showMessageDialog(login, "Maaf, User Anda tidak memiliki Akses untuk Aplikasi ini");
//            }
        } else {
            JOptionPane.showMessageDialog(login, "Maaf, User ID dan password tidak sesuai");
            d_user.setText("");
            d_pass.setText("");
            d_user.requestFocus();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        login = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        d_user = new javax.swing.JTextField();
        d_pass = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        b_masuk = new javax.swing.JButton();
        b_keluar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        tengah = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem17 = new javax.swing.JMenuItem();

        login.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        login.setTitle("LOGIN");
        login.setModal(true);
        login.setResizable(false);
        login.getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/login1.jpg"))); // NOI18N
        jPanel4.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        d_user.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_userKeyReleased(evt);
            }
        });
        jPanel4.add(d_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 150, -1));

        d_pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_passKeyReleased(evt);
            }
        });
        jPanel4.add(d_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 150, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("User ID");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Password");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, -1, 20));

        jPanel5.setBackground(new java.awt.Color(255, 51, 51));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        b_masuk.setText("Masuk");
        b_masuk.setPreferredSize(new java.awt.Dimension(83, 25));
        b_masuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_masukActionPerformed(evt);
            }
        });
        jPanel5.add(b_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, -1, -1));

        b_keluar.setText("Keluar");
        b_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_keluarActionPerformed(evt);
            }
        });
        jPanel5.add(b_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, -1));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/setting.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, -1));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 390, 50));

        login.getContentPane().add(jPanel4);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Enterprise Resource Planning | Power by Java Global Mandiri");
        setExtendedState(this.MAXIMIZED_BOTH);

        tengah.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout tengahLayout = new javax.swing.GroupLayout(tengah);
        tengah.setLayout(tengahLayout);
        tengahLayout.setHorizontalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 685, Short.MAX_VALUE)
        );
        tengahLayout.setVerticalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 520, Short.MAX_VALUE)
        );

        getContentPane().add(tengah, java.awt.BorderLayout.CENTER);

        jMenu2.setText("Penjualan");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu2.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem4.setText("1. Order Penjualan");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem6.setText("2. Cek Gudang");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuItem1.setText("3. Cetak Faktur Penjualan");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        jMenu5.setText("Pembelian");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu5.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem11.setText("1. Order Pembelian");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem11);

        jMenuItem12.setText("2. Gudang Terima Barang");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem12);

        jMenuBar1.add(jMenu5);

        jMenu3.setText("Pajak");
        jMenu3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu3.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem3.setText("Export eFaktur");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Gudang");
        jMenu4.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu4.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem7.setText("Stok Barang");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem7);

        jMenuItem8.setText("Estimasi Nilai Persediaan Barang");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem8);

        jMenuItem9.setText("Batch");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem9);

        jMenuItem10.setText("Riwayat Moving Stock");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem10);

        jMenuBar1.add(jMenu4);

        jMenu6.setText("Keuangan");
        jMenu6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu6.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem13.setText("Pelunasan Faktur Penjualan");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem13);

        jMenuItem14.setText("Pelunasan Faktur Pembelian");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem14);
        jMenu6.add(jSeparator2);

        jMenuItem5.setText("Piutang Penjualan");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem5);

        jMenuItem2.setText("Laporan Penjualan");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem2);

        jMenuBar1.add(jMenu6);

        jMenu7.setText("Master Data");
        jMenu7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenu7.setMargin(new java.awt.Insets(2, 5, 2, 10));

        jMenuItem15.setText("Data Pelanggan");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem15);

        jMenuItem16.setText("Data User");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem16);
        jMenu7.add(jSeparator3);

        jMenuItem17.setText("Data Barang");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem17);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        setBounds(0, 0, 703, 593);
    }// </editor-fold>//GEN-END:initComponents

    private void d_userKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_userKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            d_pass.requestFocus();
        }
    }//GEN-LAST:event_d_userKeyReleased

    private void d_passKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_passKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            goLogin();
        }
    }//GEN-LAST:event_d_passKeyReleased

    private void b_masukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_masukActionPerformed
        goLogin();
    }//GEN-LAST:event_b_masukActionPerformed

    private void b_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_keluarActionPerformed
        //   login.dispose();
        tutup();
    }//GEN-LAST:event_b_keluarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        login.dispose();
        setVisible(false);
        new pengaturan();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        cetakFaktur = new cetakFaktur(user, kon);
        showFrame(cetakFaktur, "Cetak Faktur");
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        exportFaktur = new exportFaktur(user, kon);
        showFrame(exportFaktur, "Export Faktur");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        SalesOrder = new SalesOrder(user, kon);
        showFrame(SalesOrder, "Sales Order");
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        laporanPiutang = new laporanPiutang(user, kon);
        showFrame(laporanPiutang, "Laporan Piutang");
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        laporanPenjualan = new laporanPenjualan(user, kon);
        showFrame(laporanPenjualan, "Laporan Penjualan");
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        cekGudang = new cekGudang();
        showFrame(cekGudang, "Cek Gudang");
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        persediaan = new persediaan();
        showFrame(persediaan, "Persediaan");
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        estimasiNilai = new estimasiNilai();
        showFrame(estimasiNilai, "Estimasi Nilai");
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        batch = new batch();
        showFrame(batch, "Batch");
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        riwayat = new riwayat();
        showFrame(riwayat, "Riwayat");
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        bayarFakturPenjualan = new bayarFakturPenjualan(user, kon);
        showFrame(bayarFakturPenjualan, "Faktur Penjualan");
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        PurchaseOrder = new PurchaseOrder(user, kon);
        showFrame(PurchaseOrder, "Purchase Order");
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        terimaBarang = new terimaBarang();
        showFrame(terimaBarang, "Terima Barang");
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        bayarFakturPembelian = new bayarFakturPembelian(user, kon);
        showFrame(bayarFakturPembelian, "Faktur Pembelian");
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        pelanggan = new pelanggan();
        showFrame(pelanggan, "Master Pelanggan");
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        salesman = new salesman();
        showFrame(salesman, "Master Salesman");
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        barang = new barang();
        showFrame(barang, "Master Barang");
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel("org.fife.plaf.Office2003.Office2003LookAndFeel");
            //   javax.swing.UIManager.setLookAndFeel("org.fife.plaf.OfficeXP.OfficeXPLookAndFeel");
            // JFrame.setDefaultLookAndFeelDecorated(true);
            new awal();

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_keluar;
    private javax.swing.JButton b_masuk;
    private javax.swing.JPasswordField d_pass;
    private javax.swing.JTextField d_user;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JDialog login;
    private javax.swing.JDesktopPane tengah;
    // End of variables declaration//GEN-END:variables
}
