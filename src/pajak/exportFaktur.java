/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pajak;

import java.io.File;
import penjualan.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import net.sf.jasperreports.engine.JasperPrint;
import utility.Item;
import utility.jasperReport;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks24
 */
public class exportFaktur extends javax.swing.JInternalFrame {

    Connection kon;
    String user;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    jasperReport jr;

    /**
     * Creates new form cetakFaktur
     */
    public exportFaktur(String user, Connection kon) {
        this.kon = kon;
        this.user = user;
        jr = new jasperReport();
        initComponents();
        komp.setJComboBoxListVektor(sales, "SELECT a.id, UPPER(b.name) as nama FROM res_users a INNER JOIN res_partner b ON b.id = a.partner_id WHERE a.active = 'true'", "-- SEMUA SALES --", kon);

        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        showData();

    }

    private void showData() {
        String awal = new java.sql.Date(tgl_mulai.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tgl_sampai.getDate().getTime()).toString();
        String unitku = ((Item) sales.getSelectedItem()).getValue().toString();
        String fsal = "AND inv.user_id = " + unitku;
        if (unitku.equalsIgnoreCase("0")) {
            fsal = "";
        }
        String sql = "SELECT inv.id,'false' as cek, inv.\"number\", inv.date_invoice, inv.date_due, \n"
                + "p.\"name\", p.city, ss.name as nama_sales, inv.amount_untaxed as jml_no ,inv.amount_tax, inv.amount_total,\n"
                + "CASE WHEN now() > inv.date_due THEN 'OVERDUE' ELSE 	concat(DATE_PART('day', inv.date_due - now())::VARCHAR, ' Hari Lagi') END as sta\n"
                + "FROM account_invoice inv\n"
                + "INNER JOIN res_partner p ON p.\"id\" = inv.partner_id\n"
                + "INNER JOIN res_users us ON us.id = inv.user_id\n"
                + "INNER JOIN res_partner ss ON ss.id = us.partner_id\n"
                + "WHERE inv.\"state\" = 'open' AND inv.residual > 0 AND inv.date_invoice >= '" + awal
                + "' AND inv.date_invoice <= '" + akhir + "' " + fsal + "\n"
                + " AND (x_npwp IS NOT NULL OR length(x_npwp) = 0) ORDER BY date_invoice";
        komp.setDataTabelCek(kon, tabelData, sql, 1, 2);
    }

    private void createCSV(String filter, String file) {

        try {
            StringBuffer str = new StringBuffer();
            str.append("\"FK\",\"KD_JENIS_TRANSAKSI\",\"FG_PENGGANTI\",\"NOMOR_FAKTUR\",\"MASA_PAJAK\",\"TAHUN_PAJAK\",\"TANGGAL_FAKTUR\",\"NPWP\",\"NAMA\",\"ALAMAT_LENGKAP\",\"JUMLAH_DPP\",\"JUMLAH_PPN\",\"JUMLAH_PPNBM\",\"ID_KETERANGAN_TAMBAHAN\",\"FG_UANG_MUKA\",\"UANG_MUKA_DPP\",\"UANG_MUKA_PPN\",\"UANG_MUKA_PPNBM\",\"REFERENSI\"\n");
            str.append("\"LT\",\"NPWP\",\"NAMA\",\"JALAN\",\"BLOK\",\"NOMOR\",\"RT\",\"RW\",\"KECAMATAN\",\"KELURAHAN\",\"KABUPATEN\",\"PROPINSI\",\"KODE_POS\",\"NOMOR_TELEPON\"\n");
            str.append("\"OF\",\"KODE_OBJEK\",\"NAMA\",\"HARGA_SATUAN\",\"JUMLAH_BARANG\",\"HARGA_TOTAL\",\"DISKON\",\"DPP\",\"PPN\",\"TARIF_PPNBM\",\"PPNBM\"\n");

            String sql = "SELECT line, id FROM v_pajak_line1 WHERE id IN(" + filter + ")";
            System.out.println(sql);
            String line2 = komp.getStringSQL(kon, "SELECT line FROM v_pajak_line2 LIMIT 1");
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String line1 = rset.getString(1);
                String id = rset.getString(2);

                str.append(line1 + "\n");
                str.append(line2 + "\n");

                PreparedStatement stat2 = kon.prepareStatement("SELECT line FROM v_pajak_line3 WHERE invoice_id = '" + id + "'");
                ResultSet rset2 = stat2.executeQuery();
                while (rset2.next()) {
                    String line3 = rset2.getString(1);
                    str.append(line3 + "\n");
                }

            }

            komp.writeFile(file, str.toString());
            pesan.pesanSukses("Sukses Export eFaktur", "Sukses Create file CSV");

        } catch (SQLException ex) {
            Logger.getLogger(exportFaktur.class.getName()).log(Level.SEVERE, null, ex);
            pesan.pesanError("GAGAL", ex.getMessage(), "Silahkan hub Administrator");
        }
    }

    private void cetakFakturPenjualan(boolean ok) {
        int yy = tabelData.getRowCount();

        String filter = "";
        if (ok) {
            for (int hh = 0; hh < yy; hh++) {
                String id = tabelData.getValueAt(hh, 1).toString();
                filter = filter + id + ",";
            }
        } else {
            for (int hh = 0; hh < yy; hh++) {
                String id = tabelData.getValueAt(hh, 1).toString();
                String cek = tabelData.getValueAt(hh, 2).toString();
                // String sal = tabelData.getValueAt(hh, 8).toString();
                if (cek.equalsIgnoreCase("true")) {
                    filter = filter + id + ",";
                }
            }
        }
       
        if (filter.length() == 0) {
            pesan.pesanError("GAGAL", "Tidak ada data yang diExport", "Pastikan ada data yg akan diexport atau dipilih");
        } else {
             String file = pilihPath();
            createCSV(filter.substring(0, filter.length() - 1), file);
        }
    }

    private String pilihPath() {
        String hasil = "";
        JFileChooser fc = new JFileChooser("namafile.");
        FileFilter filter1 = new ExtensionFileFilter("CSV File (*.csv)", new String[]{"csv"});

        fc.setFileFilter(filter1);
        int returnVal = fc.showSaveDialog(this);
        //  System.out.println(returnVal + " -- " + JFileChooser.SAVE_DIALOG);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File fileToSave = fc.getSelectedFile();
            //   System.out.println("Save as file: " + fileToSave.getAbsolutePath());
            String file = fileToSave.toString();
            if (!file.endsWith(".csv")) {
                file = file + ".csv";
            }
            hasil = file;

        }
        return hasil;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuFaktur = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        menuSurat = new javax.swing.JPopupMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnFaktur = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tgl_mulai = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        tgl_sampai = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        sales = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/category.png"))); // NOI18N
        jMenuItem1.setText("Cetak Terpilih");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        menuFaktur.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        jMenuItem2.setText("Cetak Semua");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuFaktur.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/category.png"))); // NOI18N
        jMenuItem3.setText("Cetak Terpilih");
        menuSurat.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        jMenuItem4.setText("Cetak Semua");
        menuSurat.add(jMenuItem4);

        setTitle("Export Faktur");

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Cek", "No. Faktur", "Tanggal Faktur", "Jatuh Tempo", "Nama Pelanggan", "Kota", "Sales", "Total tanpa Pajak", "Pajak", "Total dengan Pajak", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelData);

        jPanel3.add(jScrollPane1);

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 10));

        btnFaktur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/option.png"))); // NOI18N
        btnFaktur.setText("Export Faktur Penjualan");
        btnFaktur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFakturActionPerformed(evt);
            }
        });
        jPanel4.add(btnFaktur);

        jPanel1.add(jPanel4, java.awt.BorderLayout.PAGE_START);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel5, java.awt.BorderLayout.PAGE_END);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel6, java.awt.BorderLayout.LINE_END);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel7, java.awt.BorderLayout.LINE_START);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        jPanel8.setOpaque(false);
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel8.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, 90, 100));

        jLabel2.setText("s/d");
        jPanel8.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 30, 20));

        tgl_mulai.setDate(new java.util.Date());
        tgl_mulai.setDateFormatString("dd/MM/yyyy");
        tgl_mulai.setPreferredSize(new java.awt.Dimension(110, 22));
        jPanel8.add(tgl_mulai, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, -1, -1));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Tanggal :");
        jPanel8.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 80, 20));

        tgl_sampai.setDate(new java.util.Date());
        tgl_sampai.setDateFormatString("dd/MM/yyyy");
        tgl_sampai.setPreferredSize(new java.awt.Dimension(110, 22));
        jPanel8.add(tgl_sampai, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, -1));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Filter :");
        jPanel8.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 20));

        sales.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel8.add(sales, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 280, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/cari.png"))); // NOI18N
        jButton1.setText("Tampil Data");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel8.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, -1, -1));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Nama Sales :");
        jPanel8.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 90, 20));
        jPanel8.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, -1));

        jPanel2.add(jPanel8);

        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.GridLayout(1, 0, 20, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("EXPORT E-FAKTUR     ");
        jPanel9.add(jLabel6);

        jPanel2.add(jPanel9);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        showData();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFakturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFakturActionPerformed
        menuFaktur.show(btnFaktur, 0, 25);
    }//GEN-LAST:event_btnFakturActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        cetakFakturPenjualan(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        cetakFakturPenjualan(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFaktur;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPopupMenu menuFaktur;
    private javax.swing.JPopupMenu menuSurat;
    private javax.swing.JComboBox<String> sales;
    private javax.swing.JTable tabelData;
    private com.toedter.calendar.JDateChooser tgl_mulai;
    private com.toedter.calendar.JDateChooser tgl_sampai;
    // End of variables declaration//GEN-END:variables

    class ExtensionFileFilter extends FileFilter {

        String description;
        String extensions[];

        public ExtensionFileFilter(String description, String extension) {
            this(description, new String[]{extension});
        }

        public ExtensionFileFilter(String description, String extensions[]) {
            if (description == null) {
                this.description = extensions[0];
            } else {
                this.description = description;
            }
            this.extensions = (String[]) extensions.clone();
            toLower(this.extensions);
        }

        private void toLower(String array[]) {
            for (int i = 0, n = array.length; i < n; i++) {
                array[i] = array[i].toLowerCase();
            }
        }

        public String getDescription() {
            return description;
        }

        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            } else {
                String path = file.getAbsolutePath().toLowerCase();
                for (int i = 0, n = extensions.length; i < n; i++) {
                    String extension = extensions[i];
                    if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.')) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

}
