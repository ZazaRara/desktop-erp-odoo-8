/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pembelian;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import search.JTablePopupBarang;
import search.JTablePopupBeli;
import search.PopupCombo;
import search.PopupComboBarang;
import search.PopupComboBeli;
import search.PopupComponentBarang;
import search.PopupComponentBeli;
import utility.ButtonColumn;
import utility.Item;
import utility.TableCellListener;
import utility.penangananDialog;
import utility.penangananKomponen;

/**
 *
 * @author iweks24
 */
public class PurchaseOrder extends javax.swing.JInternalFrame {

    Connection kon;
    String user;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    PopupComboBeli popupCombo;
    PopupComboBarang bNama;
String userid = "1";
double batasDiskon = 5;
    /**
     * Creates new form SalesOrder
     */
    public PurchaseOrder(String user, Connection kon) {
        this.kon = kon;
        this.user = user;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);

        PopupComponentBeli buttonPopup = new JTablePopupBeli();
        popupCombo = new PopupComboBeli(buttonPopup);

        panelPelanggan.add(popupCombo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 480, 30));

        PopupComponentBarang bn = new JTablePopupBarang();
        bNama = new PopupComboBarang(bn);
        bNama.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jPanel7.add(bNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 340, 30));

        bNama.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                bNamaKu(evt);
            }
        });

        dId.setVisible(false);
        bId.setVisible(false);
        dSalesId.setVisible(false);
        setTabel();
        userid = komp.getStringSQL(kon, "SELECT id FROM res_users WHERE login = '"+user+"'");
        komp.setJComboBoxListVektor(dPembayaran, "SELECT id, name FROM account_payment_term ORDER BY id", "-- Pilih Tempo --", kon);

        Action delete = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                JTable table = (JTable) e.getSource();
                int modelRow = Integer.valueOf(e.getActionCommand());
                int y = JOptionPane.showConfirmDialog(null, "Anda yakin inging menghapus barang " + table.getValueAt(modelRow, 2) + "? Jika yakin tekan YES", "WARNING", JOptionPane.YES_OPTION);
                if (y == JOptionPane.YES_OPTION) {
                    ((DefaultTableModel) table.getModel()).removeRow(modelRow);
                    setNomor();
                }

            }
        };

        ButtonColumn buttonColumn = new ButtonColumn(tabelBarang, delete, 7);
        buttonColumn.setMnemonic(KeyEvent.VK_D);
        nilaiAwal();

         Action action = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();
                int kolom = tcl.getColumn();
                int row = tcl.getRow();
                String nilai = tcl.getNewValue().toString();
                String qty = tabelBarang.getValueAt(row, 4).toString();
                String harga = tabelBarang.getValueAt(row, 3).toString();
                String dis = tabelBarang.getValueAt(row, 5).toString();
                harga = harga.replace(".", "");
               // dis = qty.replaceAll(".000", "");
              if (kolom == 4) {
                   qty = nilai;
                } else if (kolom == 5) {
                    //   int harga_dis = Integer.parseInt(harga) - nilai;
                    dis = nilai;
                }

                double diskon = Double.parseDouble(dis);
                if (diskon > batasDiskon) {
                    pesan.pesanError("WARNING DISKON", "Maaf, Diskon yang Anda berikan terlalu besar dari batasan Diskon yang sekarusnya", "Diskon : " + String.valueOf(diskon) + "  -  Batas = " + String.valueOf(batasDiskon));
                }
                double harga_akhir = (Double.parseDouble(qty) * (Double.parseDouble(harga) - (Double.parseDouble(harga) * ((double) diskon / 100))));
                //   harga_akhir = harga_akhir + (harga_akhir * 10 / 100);
                int harga_jos = (int) harga_akhir;
                System.out.print(qty+ "  harga : "+harga+"  Disko "+diskon+" akhir  "+harga_jos);
                
                tabelBarang.setValueAt(komp.ribuan(String.valueOf(harga_jos)), row, 6);
                setNomor();
            }
        };

        TableCellListener tcl = new TableCellListener(tabelBarang, action);
    }

    private void nilaiAwal() {
        popupCombo.setText("");
        popupCombo.setVisible(true);
        komp.hapusTabel(tabelBarang);

//        dSales.setText("");
        dKota.setText("");
        dId.setText("");
        dNpwp.setText("");
        dAlamat.setText("");
 //       dLimit.setText("");
        dNamaPelanggan.setText("");

        bersihBarang();
        dTotal.setText("0");
        dPPN.setText("0");
        dDPP.setText("0");
        panelBarang.setVisible(false);
        tgl_order.setVisible(false);
        dPembayaran.setVisible(false);
        dNamaPelanggan.setVisible(false);
        btnSave.setVisible(false);
        panelOrder.setVisible(true);
        popupCombo.setText("");
        popupCombo.setVisible(true);
        btnBatal.setVisible(false);
        dNpwp.setVisible(false);
        panelPelanggan.setPreferredSize(new java.awt.Dimension(500, 324));

    }

    private void newOrder() {
        panelPelanggan.setPreferredSize(new java.awt.Dimension(260, 324));
        popupCombo.setPreferredSize(new java.awt.Dimension(240, 20));
        panelBarang.setVisible(true);
        tgl_order.setVisible(true);
        dPembayaran.setVisible(true);
        dNpwp.setVisible(true);
        btnSave.setVisible(true);
        btnBatal.setVisible(true);
        dNamaPelanggan.setVisible(true);
        panelOrder.setVisible(false);
        popupCombo.setVisible(false);
    }

    private void bNamaKu(javax.swing.event.CaretEvent evt) {
        //"Adi Purnomo#Laki#Semarang#1#32313123213#JL.Sinar Asih raya#3".split("#");
        try{
        String[] val = (String[]) bNama.getSelected();

        bHarga.setText(val[1]);
        bId.setText(val[2]);
        bQty.setText("1");
        bDiskon.setText("0");
        bJenisDiskon.setSelectedIndex(0);
        bHargaTampil.setText(komp.ribuan(val[1]));
        } catch(Exception e){
            
        }
        //  bQty.selectAll();
        //   bQty.requestFocus();
    }

    private void setTabel() {
        ((DefaultTableCellRenderer) tabelBarang.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        tabelBarang.getTableHeader().setFont(new java.awt.Font("Tahoma", Font.BOLD, 14));

        //   tabelBarang.setDefaultRenderer(Object.class, new EvenOddRenderer());
        DefaultTableCellRenderer no = new DefaultTableCellRenderer() {

        };

        tabelBarang.getColumnModel().getColumn(1).setMaxWidth(60);
        tabelBarang.getColumnModel().getColumn(1).setWidth(60);
        tabelBarang.getColumnModel().getColumn(1).setMinWidth(60);

        tabelBarang.getColumnModel().getColumn(3).setMaxWidth(120);
        tabelBarang.getColumnModel().getColumn(3).setWidth(120);
        tabelBarang.getColumnModel().getColumn(3).setMinWidth(120);

        tabelBarang.getColumnModel().getColumn(4).setMaxWidth(60);
        tabelBarang.getColumnModel().getColumn(4).setWidth(60);
        tabelBarang.getColumnModel().getColumn(4).setMinWidth(60);

        tabelBarang.getColumnModel().getColumn(5).setMaxWidth(80);
        tabelBarang.getColumnModel().getColumn(5).setWidth(80);
        tabelBarang.getColumnModel().getColumn(5).setMinWidth(80);

        tabelBarang.getColumnModel().getColumn(6).setMaxWidth(120);
        tabelBarang.getColumnModel().getColumn(6).setWidth(120);
        tabelBarang.getColumnModel().getColumn(6).setMinWidth(120);
        tabelBarang.getColumnModel().getColumn(7).setMaxWidth(50);
        tabelBarang.getColumnModel().getColumn(7).setWidth(50);
        tabelBarang.getColumnModel().getColumn(7).setMinWidth(50);
        /**
         * tabelBarang.getColumnModel().getColumn(7).setMaxWidth(0);
         * tabelBarang.getColumnModel().getColumn(7).setWidth(0);
         * tabelBarang.getColumnModel().getColumn(7).setMinWidth(0);
         *
         * tabelBarang.getColumnModel().getColumn(8).setMaxWidth(0);
         * tabelBarang.getColumnModel().getColumn(8).setWidth(0);
         * tabelBarang.getColumnModel().getColumn(8).setMinWidth(0);
         */
        tabelBarang.getColumnModel().getColumn(0).setMaxWidth(50);
        tabelBarang.getColumnModel().getColumn(0).setWidth(40);
        tabelBarang.getColumnModel().getColumn(0).setMinWidth(40);
        tabelBarang.setRowHeight(30);

        tabelBarang.getColumnModel().getColumn(0).setCellRenderer(no);
        tabelBarang.getColumnModel().getColumn(3).setCellRenderer(no);
        tabelBarang.getColumnModel().getColumn(4).setCellRenderer(no);
        tabelBarang.getColumnModel().getColumn(5).setCellRenderer(no);
        tabelBarang.getColumnModel().getColumn(6).setCellRenderer(no);

        no.setHorizontalAlignment(JLabel.CENTER);

        //   tabelBarang.setAutoResizeMode(1);
        //  ColumnsAutoSizer.sizeColumnsToFit(table);
    }

    private void setNomor() {
        int y = tabelBarang.getRowCount();
        int hasil = 0;
        for (int i = 0; i < y; i++) {
            tabelBarang.setValueAt(String.valueOf(i + 1), i, 0);
            String total = tabelBarang.getValueAt(i, 6).toString();
            total = total.replace(".", "");
            hasil = hasil + Integer.parseInt(total);
        }

        int ppn = (int) (hasil * 0.10);
        int dpp = hasil - ppn;
        dTotal.setText(komp.ribuan(hasil));
        dPPN.setText(komp.ribuan(ppn));
        dDPP.setText(komp.ribuan(dpp));
    }

    private void tambahBarang() {
        String nama = bNama.getText();
        String id = bId.getText();
        String harga = bHarga.getText();
        String diskon = bDiskon.getText();
        String qty = bQty.getText();

        if (nama.length() <= 0 || id.length() <= 0 || harga.length() <= 0 || diskon.length() <= 0 || qty.length() <= 0) {
            pesan.pesanError("GAGAL TAMBAH BARANG", "Mohon Lengkapi Data", "Monggo dicek sedoyo");
            //   bersihBarang();
        } else {
            double dis = 0;
            if (bJenisDiskon.getSelectedIndex() == 1) {
                dis = (Double.parseDouble(diskon) / Double.parseDouble(harga)) * 100;
            } else {
                dis = Double.parseDouble(diskon);
            }
            int hdis = (int) (Double.parseDouble(harga) * (dis / 100));
            int tot = Integer.parseInt(qty) * (Integer.parseInt(harga) - hdis);
            String akhirdiskon = String.valueOf(dis);
            String sub = String.valueOf(tot);
            boolean pilih = true;
            if (dis > 5) {
                int y = JOptionPane.showConfirmDialog(this, "Diskon yang diberikan lebih dari 5 %\nAnda yakin dengan diskon tersebut?", "Error", JOptionPane.YES_OPTION);
                if (y == JOptionPane.NO_OPTION) {
                    pilih = false;
                }
            }
            if (pilih) {
                DefaultTableModel model = (DefaultTableModel) tabelBarang.getModel();
                model.addRow(new Object[]{"1", id, nama, komp.ribuan(harga), qty, akhirdiskon, komp.ribuan(sub), "X"});
                bersihBarang();
                setNomor();
            }
        }
    }

    private void bersihBarang() {
        bNama.setText("");
        bId.setText("");
        bHarga.setText("0");
        bHargaTampil.setText("0");
        bDiskon.setText("0");
        bQty.setText("1");
        bJenisDiskon.setSelectedIndex(0);
        bNama.requestFocus();
    }

    private void simpanOrder() {
     int total = dNpwp.getText().trim().length();
    if (total < 3) {
          pesan.pesanError("GAGAL SIMPAN ORDER PEMBELIAN", "Nomor Faktur Belum Dimasukkan", "Mohon dimasukkan sesuai nomor faktur PEMBELIAN");
    } else {
            //    int ppn = Integer.parseInt(dPPN.getText().trim().replace(".", ""));
            //     int dpp = Integer.parseInt(dDPP.getText().trim().replace(".", ""));
            String pay = ((Item) dPembayaran.getSelectedItem()).getDescription().toString();
            if (pay.equalsIgnoreCase("-- Pilih Tempo --")) {
                pesan.pesanError("GAGAL SIMPAN ORDER PEMBELIAN", "TEMPO PEMBAYARAN BELUM DIPILIH", "silahkan pilih tempo pembayaran");
                dPembayaran.requestFocus();
            } else {
                simpanSO();
            }
       }
    }

    private int simpanProcurement(String namaSO, int so, int id) {
        int hasil = 0;

        String cus = dId.getText().trim();
        String sales = dSalesId.getText().trim();
        //       String idForm = thisProcess.getFieldValue("idForm");
        //       String namaSO = thisProcess.getFieldValue("namaSO");
       
     //   komp.setSQL(kon, "UPDATE sales_order SET procurement_group_id = '" + id + "' WHERE id = " + so);
        hasil = id;

// PICKING
        String[][] dataPicking = {{"create_date", "now()"},
        {"origin", namaSO},
        {"write_uid", "1"},
        {"recompute_pack_op", "true"},
  //      {"launch_pack_operations", "false"},
   //     {"location_id", "15"},
        {"priority", "1"},
        {"picking_type_id", "1"},
        {"partner_id", cus},
        {"move_type", "direct"},
        {"company_id", "1"},
        {"state", "assigned"},
        {"create_uid", userid},
        {"min_date", "now()"},
   //     {"printed", "false"},
        {"write_date", "now()"},
        {"date", "now()"},
        {"group_id", String.valueOf(id)},
        {"name", namaSO},
  //      {"create_date", "now()"},
   //     {"location_dest_id", "9"},
        {"max_date", "now()"},
  //      {"min_date", "now()"},
        {"auto_invoiced", "false"},
        {"invoice_state", "2binvoiced"},
     {"reception_to_invoice", "true"},
   //     {"weight_uom_id", "3"},
        };
        int idPick = komp.simpanDataReturn(kon, "stock_picking", dataPicking, false);

        int row = tabelBarang.getRowCount();

        for (int bhku = 0; bhku < row; bhku++) {
            String idBarang = tabelBarang.getValueAt(bhku, 1).toString();
            String nama = tabelBarang.getValueAt(bhku, 2).toString();
           
            String qty = tabelBarang.getValueAt(bhku, 4).toString();
            String diskon = tabelBarang.getValueAt(bhku, 5).toString();
            String hargaUnit = tabelBarang.getValueAt(bhku, 3).toString().replace(".", "");
            //      String hargaUnitPajak = tab.getValueAt(bhku, "txtunitpricepajak").toString();
            String uom = komp.getStringSQL(kon, "SELECT b.uom_id FROM product_product a INNER JOIN product_template b ON b.id = a.product_tmpl_id WHERE a.id = " + idBarang);

            String[][] data2 = {{"create_date", "now()"},
            {"product_uom", uom},
            {"price_unit", hargaUnit},
          {"product_qty", qty},
    //        {"product_uom_qty", qty},
            //          {"price_subtotal", total},
    //        {"delay", "2"},
            {"write_uid", "1"},
            {"invoiced", "false"},
            //       {"currency_id", "3"},
            //           {"price_reduce_taxexcl", hargaUnit},
            {"create_uid", userid},
            //           {"price_tax", pajaku},
            //          {"qty_to_invoice", "0"},
            //          {"customer_lead", "0"},
            //          {"layout_category_sequence", "0"},
            {"company_id", "1"},
            {"partner_id", cus},
            {"state", "confirmed"},
            {"order_id", String.valueOf(so)},
            //        {"qty_invoiced", "0"},
       //     {"sequence", String.valueOf(10)},
            {"discount", diskon},
            {"write_date", "now()"},
  //          {"price_reduce", hargaUnit},
    //        {"qty_delivered", diskon},
            {"product_id", idBarang},
            //       {"price_reduce_taxinc", hargaUnitPajak},
            //       {"price_total", totalPajak},
            //        {"invoice_status", "no"},
            {"name", nama},
       {"date_planned", "now()"},
            };
            int no = komp.simpanDataReturn(kon, "purchase_order_line", data2, false);
       //     String inspajak = "INSERT INTO sale_order_tax(order_line_id, tax_id) VALUES(" + String.valueOf(no) + ",7)";
      //      komp.setSQL(kon, inspajak);

//            String[][] dataPO = {{"create_date", "now()"},
//            {"origin", namaSO},
//            {"product_uom", uom},
//            {"product_uos", uom},
//            {"product_uos_qty", qty},
//            {"product_qty", qty},
//            {"write_uid", userid},
//            {"create_uid", userid},
//            {"company_id", "1"},
//            {"priority", "1"},
//            {"state", "running"},
//            {"write_date", "now()"},
//            {"name", nama},
//            {"product_id", idBarang},
//            {"group_id", String.valueOf(id)},
//            {"date_planned", "now()"},
//            {"rule_id", "1"},
//            {"sale_line_id", String.valueOf(no)},
//            {"location_id", "9"},
//            {"partner_dest_id", cus},
//            {"invoice_state", "2binvoiced"},
//            {"warehouse_id", "1"},};
//            int idsal = komp.simpanDataReturn(kon, "procurement_order", dataPO, false);
//            //   String hargaUnit = tab.getValueAt(bhku, "txtunitprice").toString();

            String[][] data5 = {{"create_date", "now()"},
            {"origin", namaSO},
            {"product_uom", uom},
            {"product_uos", uom},
            {"price_unit", hargaUnit},
            {"product_uos_qty", qty},
            {"product_uom_qty", qty},
            {"procure_method", "make_to_stock"},
            {"product_qty", qty},
     //       {"partner_id", cus},
            {"priority", "1"},
            {"picking_type_id", "1"},
            {"location_id", "8"},
      //      {"sequence", "10"},
            {"company_id", "1"},
            {"state", "assigned"},
    //        {"ordered_qty", qty},
            {"date_expected", "now()"},
            {"purchase_line_id", String.valueOf(no)},
            {"create_uid", userid},
            {"warehouse_id", "1"},
            {"partially_available", "false"},
            {"propagate", "true"},
            {"date", "now()"},
      //      {"scrapped", "false"},
            {"write_uid", "1"},
            {"product_id", idBarang},
            {"name", nama},
  //          {"rule_id", "1"},
            {"location_dest_id", "12"},
            {"write_date", "now()"},
            {"group_id", String.valueOf(id)},
            {"picking_id", String.valueOf(idPick)},
            {"invoice_state", "2binvoiced"}, //          {"to_refund_so", "false"},
        //          {"weight", "0"},
        //          {"weight_uom_id", "3"},
        };
            komp.simpanDataReturn(kon, "stock_move", data5, false);
        }
        return hasil;
    }

    private int simpanSO() {
        int hasil = 0;
        String sales = dSalesId.getText().trim();
        String tgl = new java.sql.Date(tgl_order.getDate().getTime()).toString();
        String cus = dId.getText().trim();
        String idForm = dNpwp.getText();// "ORDER/" + tgl + "/" + cus;
        String tot = dTotal.getText().trim().replace(".", "");
        String pajak = dPPN.getText().trim().replace(".", "");
        String sub = dDPP.getText().trim().replace(".", "");
        String pay = ((Item) dPembayaran.getSelectedItem()).getValue().toString();
        String npwp = dNpwp.getText().trim();

         String[][] dataPO = {{"create_date", "now()"},
        {"name", idForm},
        {"move_type", "direct"},
        {"write_uid", userid},
        {"create_uid", userid},
        {"partner_id", "1"},
        {"write_date", "now()"},};
        int id = komp.simpanDataReturn(kon, "procurement_group", dataPO, false);
        
        String[][] data = {{"create_date", "now()"},
        {"write_uid", "1"},
        {"write_date", "now()"},
        //      {"team_id", "1"},
        {"date_approve", tgl},
        {"date_order", tgl},
        {"partner_id", cus},
        {"amount_untaxed", tot},
        {"company_id", "1"},
        {"payment_term_id", pay},
        {"state", "approved"},
        {"amount_tax", "0"},
        {"pricelist_id", "2"},
        {"invoice_method", "picking"},
        {"amount_total", tot},
        {"journal_id", "2"},
        {"auto_invoiced", "false"},
        {"auto_invoice", "valid"},
        {"name", idForm},
        {"location_id", "12"},
        {"picking_type_id", "1"},
        {"currency_id", "13"},
        {"message_last_post", "now()"},
        {"shipped", "false"},
        {"validator", "1"},
   {"minimum_planned_date", "now()"},
        {"create_uid", userid},
       // {"x_npwp", npwp},
        };
        hasil = komp.simpanDataReturn(kon, "purchase_order", data, false);
        if (hasil > 0) {
            int yy = simpanProcurement(idForm, hasil, id);
            if (yy > 0) {
                pesan.pesanSukses("SUKSES SIMPAN ORDER PEMBELIAN", "NO ORDER : " + idForm);
                nilaiAwal();
            }
        }
        return hasil;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pop_menu = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        panelPelanggan = new javax.swing.JPanel();
        dNamaPelanggan = new javax.swing.JTextField();
        panelOrder = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        dNpwp = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dKota = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        dId = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        dAlamat = new javax.swing.JTextArea();
        dPembayaran = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        dTotal = new javax.swing.JLabel();
        dPPN = new javax.swing.JLabel();
        dDPP = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        label2 = new javax.swing.JLabel();
        label3 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        btnSave = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        tgl_order = new com.toedter.calendar.JDateChooser();
        jSeparator4 = new javax.swing.JSeparator();
        btnBatal = new javax.swing.JButton();
        dSalesId = new javax.swing.JTextField();
        panelBarang = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelBarang = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        bHarga = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        bQty = new javax.swing.JTextField();
        bDiskon = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        bJenisDiskon = new javax.swing.JComboBox<>();
        bHargaTampil = new javax.swing.JLabel();
        btnTambah = new javax.swing.JButton();
        bId = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();

        jMenuItem1.setBackground(new java.awt.Color(204, 255, 204));
        jMenuItem1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        jMenuItem1.setText("HAPUS BARANG");
        pop_menu.add(jMenuItem1);

        setTitle("PURCHASE ORDER");

        panelPelanggan.setBackground(new java.awt.Color(204, 102, 255));
        panelPelanggan.setPreferredSize(new java.awt.Dimension(260, 324));
        panelPelanggan.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dNamaPelanggan.setEditable(false);
        dNamaPelanggan.setBackground(new java.awt.Color(255, 204, 204));
        dNamaPelanggan.setBorder(null);
        dNamaPelanggan.setOpaque(false);
        panelPelanggan.add(dNamaPelanggan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 240, -1));

        panelOrder.setBackground(new java.awt.Color(204, 102, 255));
        panelOrder.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 204));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/calculate.png"))); // NOI18N
        jButton1.setText("ORDER BARU");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panelOrder.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        panelPelanggan.add(panelOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 530, 570));

        dNpwp.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        dNpwp.setBorder(null);
        panelPelanggan.add(dNpwp, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 240, 30));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel1.setText("Nomor Faktur");
        panelPelanggan.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 160, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("SUPPLIER");
        panelPelanggan.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 160, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel4.setText("ALAMAT");
        panelPelanggan.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 160, -1));

        dKota.setEditable(false);
        dKota.setBackground(new java.awt.Color(255, 204, 204));
        dKota.setBorder(null);
        dKota.setOpaque(false);
        panelPelanggan.add(dKota, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 240, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel5.setText("KOTA");
        panelPelanggan.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 160, -1));

        dId.setEditable(false);
        panelPelanggan.add(dId, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 720, 40, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel6.setText("TANGGAL ORDER");
        panelPelanggan.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 160, -1));

        jScrollPane2.setBorder(null);
        jScrollPane2.setOpaque(false);

        dAlamat.setEditable(false);
        dAlamat.setBackground(new java.awt.Color(204, 102, 255));
        dAlamat.setColumns(20);
        dAlamat.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        dAlamat.setLineWrap(true);
        dAlamat.setRows(2);
        dAlamat.setTabSize(0);
        dAlamat.setWrapStyleWord(true);
        dAlamat.setBorder(null);
        jScrollPane2.setViewportView(dAlamat);

        panelPelanggan.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 240, -1));

        dPembayaran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        panelPelanggan.add(dPembayaran, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 230, -1));
        panelPelanggan.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 400, 10));

        dTotal.setBackground(new java.awt.Color(0, 0, 204));
        dTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        dTotal.setForeground(new java.awt.Color(0, 0, 153));
        dTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dTotal.setText("XXXXX");
        panelPelanggan.add(dTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 350, 190, -1));

        dPPN.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dPPN.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dPPN.setText("XXXXX");
        panelPelanggan.add(dPPN, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 420, 190, -1));

        dDPP.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dDPP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dDPP.setText("XXXXXX");
        panelPelanggan.add(dDPP, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, 200, -1));

        label1.setBackground(new java.awt.Color(0, 0, 204));
        label1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label1.setForeground(new java.awt.Color(0, 0, 153));
        label1.setText("TOTAL");
        label1.setPreferredSize(new java.awt.Dimension(40, 29));
        panelPelanggan.add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 100, -1));

        label2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label2.setText("PPN");
        panelPelanggan.add(label2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, -1, -1));

        label3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label3.setText("DPP");
        panelPelanggan.add(label3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, -1, -1));

        jSeparator2.setForeground(new java.awt.Color(153, 153, 153));
        panelPelanggan.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 560, 390, 2));

        btnSave.setBackground(new java.awt.Color(0, 255, 51));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/download.png"))); // NOI18N
        btnSave.setText("SIMPAN ORDER PEMBELIAN");
        btnSave.setBorder(null);
        btnSave.setBorderPainted(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        panelPelanggan.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, 240, 30));
        panelPelanggan.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 400, 30));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel12.setText("TEMPO PEMBAYARAN");
        panelPelanggan.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 160, -1));

        tgl_order.setDate(new java.util.Date());
        tgl_order.setDateFormatString("dd/MM/yyyy");
        tgl_order.setPreferredSize(new java.awt.Dimension(110, 22));
        panelPelanggan.add(tgl_order, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, -1, -1));

        jSeparator4.setForeground(new java.awt.Color(153, 153, 153));
        panelPelanggan.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 390, 30));

        btnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/batal.png"))); // NOI18N
        btnBatal.setText("BATAL ORDER");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        panelPelanggan.add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 510, -1, -1));

        dSalesId.setEditable(false);
        panelPelanggan.add(dSalesId, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 720, 40, -1));

        getContentPane().add(panelPelanggan, java.awt.BorderLayout.LINE_START);

        panelBarang.setBackground(new java.awt.Color(255, 255, 255));
        panelBarang.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        tabelBarang.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tabelBarang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "ID", "Nama Barang", "Harga", "Qty", "Diskon", "Total", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, true, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelBarang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelBarangMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelBarang);

        jPanel6.add(jScrollPane1);

        panelBarang.add(jPanel6, java.awt.BorderLayout.CENTER);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("HARGA SATUAN");
        jPanel7.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, 110, -1));
        jPanel7.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 0, 30, 120));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel9.setText("NAMA BARANG");
        jPanel7.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 160, -1));

        bHarga.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        bHarga.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        bHarga.setText("0");
        bHarga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bHargaKeyReleased(evt);
            }
        });
        jPanel7.add(bHarga, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 110, 30));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("QTY");
        jPanel7.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 20, 60, -1));

        bQty.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        bQty.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        bQty.setText("1");
        bQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bQtyKeyReleased(evt);
            }
        });
        jPanel7.add(bQty, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 40, 60, 30));

        bDiskon.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        bDiskon.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        bDiskon.setText("0");
        bDiskon.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                bDiskonFocusGained(evt);
            }
        });
        bDiskon.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bDiskonKeyReleased(evt);
            }
        });
        jPanel7.add(bDiskon, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 40, 60, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("DISKON");
        jPanel7.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, 60, -1));

        bJenisDiskon.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        bJenisDiskon.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "%", "Rp" }));
        jPanel7.add(bJenisDiskon, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 70, 60, 30));

        bHargaTampil.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        bHargaTampil.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        bHargaTampil.setText("0");
        jPanel7.add(bHargaTampil, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, 110, 30));

        btnTambah.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnTambah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/import.png"))); // NOI18N
        btnTambah.setText("TAMBAH");
        btnTambah.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTambah.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });
        jPanel7.add(btnTambah, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 90, 60));
        jPanel7.add(bId, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 20, 60, -1));

        panelBarang.add(jPanel7, java.awt.BorderLayout.PAGE_START);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setPreferredSize(new java.awt.Dimension(10, 324));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 632, Short.MAX_VALUE)
        );

        panelBarang.add(jPanel9, java.awt.BorderLayout.LINE_END);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setPreferredSize(new java.awt.Dimension(10, 324));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 632, Short.MAX_VALUE)
        );

        panelBarang.add(jPanel11, java.awt.BorderLayout.WEST);

        getContentPane().add(panelBarang, java.awt.BorderLayout.CENTER);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(jPanel3, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bHargaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bHargaKeyReleased
        bHargaTampil.setText(komp.ribuan(bHarga.getText().trim()));
    }//GEN-LAST:event_bHargaKeyReleased

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        tambahBarang();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void bDiskonFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_bDiskonFocusGained
        bDiskon.selectAll();
    }//GEN-LAST:event_bDiskonFocusGained

    private void bQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bQtyKeyReleased
        komp.setJTextHarusIsi(evt, bQty, bDiskon);
    }//GEN-LAST:event_bQtyKeyReleased

    private void bDiskonKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bDiskonKeyReleased
        komp.setJTextHarusIsiTextToButton(evt, bQty, btnTambah);
    }//GEN-LAST:event_bDiskonKeyReleased

    private void tabelBarangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelBarangMouseClicked
        if (tabelBarang.getSelectedRow() >= 0) {
            if (evt.getButton() == evt.BUTTON3) {
                pop_menu.show(tabelBarang, evt.getX(), evt.getY());
            }
        }
    }//GEN-LAST:event_tabelBarangMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String a = popupCombo.getText();
        if (a.equalsIgnoreCase("") || a.length() == 0) {
            pesan.pesanError("DATA KOSONG", "SUPPLIER BELUM DIPILIH", "Mohon pilih supplier sebelum membuat order baru");
        } else {
            String[] val = (String[]) popupCombo.getSelected();
//            dSales.setText(val[1]);
            dKota.setText(val[2]);
            dId.setText(val[3]);
//            dNpwp.setText(val[4]);
            dAlamat.setText(val[5]);
 //           dLimit.setText(val[6]);
            dSalesId.setText(val[7]);
            dNamaPelanggan.setText(val[0]);
            newOrder();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        int y = JOptionPane.showConfirmDialog(this, "Anda yakin ingin membatal Order ini?\nData akan dibatalkan dan tidak dapat dikembalikan\nJika Yakin silahkan tekan YES", "Konfirmasi Batal Order", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            nilaiAwal();
        }
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        int y = JOptionPane.showConfirmDialog(this, "Anda yakin ingin melakukan penyimpanan Order ini?\nJika Yakin silahkan tekan YES", "Simpan Order", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            simpanOrder();
        }
    }//GEN-LAST:event_btnSaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField bDiskon;
    private javax.swing.JTextField bHarga;
    private javax.swing.JLabel bHargaTampil;
    private javax.swing.JTextField bId;
    private javax.swing.JComboBox<String> bJenisDiskon;
    private javax.swing.JTextField bQty;
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTambah;
    private javax.swing.JTextArea dAlamat;
    private javax.swing.JLabel dDPP;
    private javax.swing.JTextField dId;
    private javax.swing.JTextField dKota;
    private javax.swing.JTextField dNamaPelanggan;
    private javax.swing.JTextField dNpwp;
    private javax.swing.JLabel dPPN;
    private javax.swing.JComboBox<String> dPembayaran;
    private javax.swing.JTextField dSalesId;
    private javax.swing.JLabel dTotal;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JLabel label3;
    private javax.swing.JPanel panelBarang;
    private javax.swing.JPanel panelOrder;
    private javax.swing.JPanel panelPelanggan;
    private javax.swing.JPopupMenu pop_menu;
    private javax.swing.JTable tabelBarang;
    private com.toedter.calendar.JDateChooser tgl_order;
    // End of variables declaration//GEN-END:variables

}
