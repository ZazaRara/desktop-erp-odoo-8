/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.getConnection;
import utility.penangananKomponen;

public class JTablePopup extends JTable implements PopupComponent {

    private JTablePopup self;
    private PopupCombo theCombo;
    private JScrollPane scrollPane;
    private DefaultTableModel tableModel;
    private String[] selected = null;

    public JTablePopup() {
        super();
        self = this;
        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int r, int c) {
                return false;
            }
        };
        tableModel.setColumnIdentifiers(new String[]{"Nama Pelanggan", "Sales", "Kota", "ID", "N", "A", "P", "SA"});

        setModel(tableModel);
        this.getColumn("ID").setMinWidth(0);
        this.getColumn("ID").setMaxWidth(0);
        this.getColumn("ID").setWidth(0);
        this.getColumn("Kota").setMinWidth(100);
        this.getColumn("Kota").setMaxWidth(100);
        this.getColumn("Kota").setWidth(100);
        this.getColumn("Sales").setMinWidth(120);
        this.getColumn("Sales").setMaxWidth(120);
        this.getColumn("Sales").setWidth(100);
        this.getColumn("N").setMinWidth(0);
        this.getColumn("N").setMaxWidth(0);
        this.getColumn("N").setWidth(0);
        this.getColumn("A").setMinWidth(0);
        this.getColumn("A").setMaxWidth(0);
        this.getColumn("A").setWidth(0);
        this.getColumn("P").setMinWidth(0);
        this.getColumn("P").setMaxWidth(0);
        this.getColumn("P").setWidth(0);
        this.getColumn("SA").setMinWidth(0);
        this.getColumn("SA").setMaxWidth(0);
        this.getColumn("SA").setWidth(0);
        scrollPane = new JScrollPane(self);
        initEvents();
        getData();
        initData("");
    }

    private void initEvents() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                JTable table = (JTable) e.getSource();
                Point p = e.getPoint();
                int row = table.rowAtPoint(p);
                if (row < 0) {
                    return;
                }

                int cols = getColumnModel().getColumnCount();
                selected = new String[cols];
                for (int i = 0; i < cols; i++) {
                    selected[i] = getValueAt(row, i).toString();
                }
                theCombo.setText(selected[0]);

                // if double click
                if (e.getClickCount() == 2) {
                    theCombo.hidePopup();
                }
            }
        });
    }

    private String[][] sample;

    private void getData() {
        try {
            getConnection kb = new getConnection();
            penangananKomponen pn = new penangananKomponen();
            Connection kon = kb.getConnection(kb.jdbc, kb.url, kb.user, kb.pass);
            int ak = pn.getIntSQL(kon, "SELECT count(1) as jml FROM res_partner WHERE customer = true AND user_id IS NOT NULL");
            sample = new String[ak][];
            String sql = "SELECT a.name, b.name as sales, a.city, a.id,a.vat, "
                    + "a.street, CASE WHEN a.credit_limit IS NULL OR a.credit_limit <= 1000 THEN 0::bigint ELSE a.credit_limit::bigint END as limit,"
                    + "a.user_id as salesid "
                    + "FROM res_partner a INNER JOIN res_users r ON r.id = a.user_id INNER JOIN res_partner b ON b.id = r.partner_id WHERE a.customer = true ORDER BY a.name";
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int i = 0;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
                String e = rset.getString(5);
                String f = rset.getString(6);
                String g = rset.getString(7);
                String y = rset.getString(8);

                String hasil = a + "#" + b + "#" + c + "#" + d + "#" + e + "#" + f + "#" + g + "#" + y;
                //   System.out.println(hasil);
                sample[i] = hasil.split("#");
                i++;
            }

        } catch (SQLException ex) {
            Logger.getLogger(JTablePopup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initData(String filter) {
        //"Nama Pelanggan", "Salesman", "Kota", "ID", "N", "A", "P"
        //   sample[0] = "Adi Purnomo#Laki#Semarang#1#32313123213#JL.Sinar Asih raya#3".split("#");
        //   sample[1] = "Adi Purnomo#Laki#Semarang#1#32313123213#JL.Sinar Asih raya#3".split("#");
        tableModel.setRowCount(0);
        for (String[] data : sample) {
            if (filter.isEmpty() || data[0].toLowerCase().contains(filter.toLowerCase())) {
                tableModel.addRow(data);
            }
        }
    }

    public boolean getScrollableTracksViewportHeight() {
        if (getParent() instanceof JViewport) {
            return getParent().getHeight() > getPreferredSize().height;
        }

        return super.getScrollableTracksViewportHeight();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (getRowCount() < 1) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setColor(Color.BLACK);
            g2.drawString("Data tidak ditemukan.", 10, getTableHeader().getHeight());
        }
    }

    @Override
    public JComponent getComponent() {
        return scrollPane;
    }

    @Override
    public void filter(String search) {
        initData(search);
    }

    @Override
    public void onPopShown() {
        // popup is shown,
    }

    @Override
    public void onPopHidden() {
        // popup is hidden
    }

    @Override
    public void setPopupCombo(PopupCombo popupCombo) {
        theCombo = popupCombo;
    }

    @Override
    public Object getSelected() {
        return selected;
    }
}
