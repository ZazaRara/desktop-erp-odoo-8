/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableCellRenderer;
import utility.getConnection;
import utility.penangananKomponen;

public class JTablePopupBarang extends JTable implements PopupComponentBarang {

    private JTablePopupBarang self;
    private PopupComboBarang theCombo;
    private JScrollPane scrollPane;
    private DefaultTableModel tableModel;
    private String[] selected = null;

    public JTablePopupBarang() {
        super();
        self = this;
        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int r, int c) {
                return false;
            }
        };
        tableModel.setColumnIdentifiers(new String[]{"Nama Barang", "Harga", "ID", "Stok"});

        setModel(tableModel);
        this.getColumn("ID").setMinWidth(0);
        this.getColumn("ID").setMaxWidth(0);
        this.getColumn("ID").setWidth(0);
        this.getColumn("Harga").setMinWidth(55);
        this.getColumn("Harga").setMaxWidth(55);
        this.getColumn("Harga").setWidth(55);
        this.getColumn("Stok").setMinWidth(40);
        this.getColumn("Stok").setMaxWidth(40);
        this.getColumn("Stok").setWidth(40);
        DefaultTableCellRenderer no = new DefaultTableCellRenderer() {

            public void setBackground(Color v) {

                super.setBackground(java.awt.SystemColor.activeCaption);
                //   super.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
            }
        };

        this.getColumn("Harga").setCellRenderer(no);
        this.getColumn("Stok").setCellRenderer(no);
        no.setHorizontalAlignment(JLabel.RIGHT);
        scrollPane = new JScrollPane(self);
        initEvents();
        getData();
        initData("");
    }

    private void initEvents() {
        addKeyListener(new KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent e) {
//                if (e.getKeyCode() == e.VK_ENTER) {
//                    JTable table = (JTable) e.getSource();
//                    //   Point p = e.getPoint();
//                    int row = table.getSelectedRow();
//                    if (row < 0) {
//                        return;
//                    }
//
//                    int cols = getColumnModel().getColumnCount();
//                    selected = new String[cols];
//                    for (int i = 0; i < cols; i++) {
//                        selected[i] = getValueAt(row, i).toString();
//                    }
//                    theCombo.setText(selected[0]);
//
//                    // if double click
//                    theCombo.hidePopup();
//                }
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override

            public void mousePressed(MouseEvent e) {
                JTable table = (JTable) e.getSource();
                Point p = e.getPoint();
                int row = table.rowAtPoint(p);
                if (row < 0) {
                    return;
                }

                int cols = getColumnModel().getColumnCount();
                selected = new String[cols];
                for (int i = 0; i < cols; i++) {
                    selected[i] = getValueAt(row, i).toString();
                }
                theCombo.setText(selected[0]);

                // if double click
                if (e.getClickCount() == 2) {
                    theCombo.hidePopup();
                }
            }
        });
    }

    private String[][] sample;

    public void getData() {
        try {
            getConnection kb = new getConnection();
            penangananKomponen pn = new penangananKomponen();
            Connection kon = kb.getConnection(kb.jdbc, kb.url, kb.user, kb.pass);
            int ak = pn.getIntSQL(kon, "SELECT count(1) as jml FROM product_product WHERE active = true AND length(name_template) > 1");
            sample = new String[ak][];
            String sql = "SELECT a.name_template, b.list_price::BIGINT as harga,a.id, "
                    + "(CASE WHEN c.stock_akhir IS NULL THEN 0 ELSE c.stock_akhir END)::BIGINT as stok "
                    + "FROM product_product a "
                    + "INNER JOIN product_template b ON b.id = a.product_tmpl_id "
                    + "LEFT JOIN v_stok c ON c.id_product = a.id "
                    + "WHERE a.active = true AND length(a.name_template) > 1 ORDER BY a.name_template";
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int i = 0;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
//                String d = rset.getString(4);
//                String e = rset.getString(5);
//                String f = rset.getString(6);
//                String g = rset.getString(7);

                String hasil = a + "#" + b + "#" + c + "#" + d;
                //   System.out.println(hasil);
                sample[i] = hasil.split("#");
                i++;
            }

        } catch (SQLException ex) {
            Logger.getLogger(JTablePopupBarang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initData(String filter) {
        //"Nama Pelanggan", "Salesman", "Kota", "ID", "N", "A", "P"
        //   sample[0] = "Adi Purnomo#Laki#Semarang#1#32313123213#JL.Sinar Asih raya#3".split("#");
        //   sample[1] = "Adi Purnomo#Laki#Semarang#1#32313123213#JL.Sinar Asih raya#3".split("#");
        tableModel.setRowCount(0);
        for (String[] data : sample) {
            if (filter.isEmpty() || data[0].toLowerCase().contains(filter.toLowerCase())) {
                tableModel.addRow(data);
            }
        }
    }

    public boolean getScrollableTracksViewportHeight() {
        if (getParent() instanceof JViewport) {
            return getParent().getHeight() > getPreferredSize().height;
        }

        return super.getScrollableTracksViewportHeight();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (getRowCount() < 1) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setColor(Color.BLACK);
            g2.drawString("Data tidak ditemukan.", 10, getTableHeader().getHeight());
        }
    }

    @Override
    public JComponent getComponent() {
        return scrollPane;
    }

    @Override
    public void filter(String search) {
        initData(search);
    }

    @Override
    public void onPopShown() {
        // popup is shown,
    }

    @Override
    public void onPopHidden() {
        // popup is hidden
    }

    @Override
    public void setPopupComboBarang(PopupComboBarang popupCombo) {
        theCombo = popupCombo;
    }

    @Override
    public Object getSelected() {
        return selected;
    }
}
