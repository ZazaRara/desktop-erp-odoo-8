/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search;

import javax.swing.*;


public interface PopupComponentBarang {
	JComponent getComponent();
	void filter(String search);
	void onPopShown();
	void onPopHidden();
	void setPopupComboBarang(PopupComboBarang popupCombo);
	Object getSelected();
        void getData();
}
