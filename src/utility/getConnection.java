/*
 * getConnection.java
 *
 * Created on 05 April 2009, 9:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;

/**
 *
 * @author Laptop Rara
 */
public class getConnection {

    private static final String ALGO = "AES";
    private static final byte[] keyValue
            = new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't',
                'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
    public String jdbc = "";
    public String url = "";
    public String user = "";
    public String pass = "";
    public String ip = "";
    public String db = "";

    /**
     * Creates a new instance of getConnection
     */
    public getConnection() {

        String folder = System.getProperty("user.dir");
        System.out.println(folder);
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(folder + "\\Laporan\\config.properties");

            // load a properties file
            prop.load(input);
            jdbc = decrypt(prop.getProperty("driver"));
            //     String url = prop.getProperty("url");
            user = decrypt(prop.getProperty("user"));
            ip = decrypt(prop.getProperty("ip"));
            db = decrypt(prop.getProperty("nama"));
            pass = decrypt(prop.getProperty("pass"));
            url = decrypt(prop.getProperty("url"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static String decrypt(String encryptedData) {
        String hasil = "";
        try {
            Key key = new SecretKeySpec(keyValue, ALGO);
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            hasil = decryptedValue;
        } catch (NoSuchAlgorithmException ex) {
            // Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            //   Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            //   Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //   Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            //   Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            //   Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    public Connection getConnection(String driverJDBC, String url, String user, String password) {
        boolean driver = false;
        Connection koneksi = null;
        try {
            // Penyambungan Driver
            Class.forName(driverJDBC);
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            koneksi = null;
            System.out.println(not);
        }

        if (driver == true) {

            try {
                koneksi = java.sql.DriverManager.getConnection(url, user, password);

            } // Akhir try
            catch (java.sql.SQLException sqln) {
                koneksi = null;
                System.out.println(sqln);

            }
        }

        return koneksi;
    }
}
