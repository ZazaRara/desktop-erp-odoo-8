/*
 * penangananDialog.java
 *
 * Created on 27 Juli 2008, 12:41
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package utility;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

/**
 *
 * @author Rara Kaltsum
 */
public class penangananDialog extends JOptionPane {

    /**
     * Creates a new instance of penangananDialog
     */
    public penangananDialog() {
    }

    public void tutupInternalFrame(JInternalFrame internal, JToolBar toolbar) {
        String pesan = "<html>Anda Yakin ingin menutup Form<br><b>" + internal.getTitle().toUpperCase() + " ??<br></b></html>";
        int p = this.showConfirmDialog(internal, pesan, "Konfirmasi Tutup Form", this.YES_NO_OPTION);
        if (p == 0) {
            internal.dispose();
            toolbar.setVisible(false);
        }
    }

    public void pesanError(String judul, String detail, String tiga) {
        String pesan = "<html><b>" + judul + "<br /><font color=\"#FF0000\">Detail : " + detail.replace("\n", " - ") + "</font><br />" + tiga + "</b></html>";
        JOptionPane.showMessageDialog(null, pesan, "Error Message", JOptionPane.ERROR_MESSAGE);
    }

    public void pesanSukses(String judul, String detail) {
        String pesan = "<html><b>" + judul + "<br /><font color = \"#0000FF\">" + detail + "</font></b></html>";
        JOptionPane.showMessageDialog(null, pesan, "Success Message", JOptionPane.INFORMATION_MESSAGE);
    }
}
