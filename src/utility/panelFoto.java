/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author iweks
 */
public class panelFoto extends JPanel {

    BufferedImage image;
    Dimension size;
    public Rectangle clip;
    boolean showClip;
    private static final int IMG_WIDTH = 640;
    private static final int IMG_HEIGHT = 480;
    int zoom = 5;
    int xx = 0;
    int yy = 0;

    public panelFoto(BufferedImage image) {

        this.image = image;
        size = new Dimension(image.getWidth(), image.getHeight());
        showClip = true;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int x = (getWidth() - size.width) / 2;
        int y = (getHeight() - size.height) / 2;
        //    g2.drawImage(image, x, y, this);
        g2.drawImage(image, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        if (showClip) {
            if (clip == null) {
                createClip();
            }
            g2.setPaint(Color.red);
            g2.draw(clip);
        }
    }

    public void setClip(int x, int y) {
        // keep clip within raster
        xx = x;
        yy = y;
        int x0 = (getWidth() - size.width) / 2;
        int y0 = (getHeight() - size.height) / 2;
        if (x < x0 || x + clip.width > x0 + size.width
                || y < y0 || y + clip.height > y0 + size.height) {
            return;
        }
        clip.setLocation(x, y);
        repaint();
    }

    public Dimension getPreferredSize() {
        return size;
    }

    public void setZoom(int bh) {
        zoom = bh;
        // createClip();
        clip = new Rectangle(60 * zoom, 80 * zoom);
        clip.setLocation(xx, yy);
        repaint();
    }

    private void createClip() {
        clip = new Rectangle(60 * zoom, 80 * zoom);
        clip.x = (getWidth() - clip.width) / 2;
        clip.y = (getHeight() - clip.height) / 2;
    }

    public BufferedImage clipImage() {
        BufferedImage clipped = null;
        try {
            int w = clip.width;
            int h = clip.height;
            int x0 = (getWidth() - size.width) / 2;
            int y0 = (getHeight() - size.height) / 2;
            int x = clip.x - x0;
            int y = clip.y - y0;
            clipped = image.getSubimage(x, y, w, h);
            //    System.out.println("ukuran w : " + w + " - h : " + h + " -- x : " + x0 + "  y : " + y0);
        } catch (RasterFormatException rfe) {
            System.out.println("raster format error: " + rfe.getMessage());
            return clipped;
        }
        return clipped;
    }

}


