/*
 * penangananKomponen.java
 *
 * Created on 21 Juli 2008, 22:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package utility;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.JTextComponent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * @author Rara Kaltsum
 */
public class penangananKomponen {

    /**
     * Creates a new instance of penangananKomponen
     */
    public penangananKomponen() {
    }

    public void createHeaderTabelExcel(String[][] data, HSSFSheet sheet, HSSFRow row, HSSFCell cell, CellStyle cs3, int bb) {
        int y = data.length;
        row = sheet.createRow(bb);
        for (int x = 0; x < y; x++) {
            Object xx = data[x][0];
            if (xx != null) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                sheet.setColumnWidth((short) (x), (short) (short) ((50 * Integer.parseInt(isi)) / ((double) 1 / 20)));
                cell = row.createCell(x);
                cell.setCellStyle(cs3);
                cell.setCellValue(kolom);
            }
        }
    }

    public void writeFile(String file, String isi) {

        try {
            byte[] b = isi.getBytes();
            File outputx = new File(file);
            RandomAccessFile random = new RandomAccessFile(outputx, "rw");

            random.seek(outputx.length());
            random.write(b);
            random.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public void setTabelExcel(Connection kon, String Query, HSSFSheet sheet, HSSFRow rowx, HSSFCell cell, int bb) {
        try {

            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            int columns = rs.getMetaData().getColumnCount();
            //  System.out.println(columns);
            int yh = 1;
            while (rs.next()) {
                rowx = sheet.createRow(bb);
                cell = rowx.createCell(0);
                cell.setCellValue(yh);
                for (int i = 1; i <= columns; i++) {
                    String data = rs.getObject(i).toString();
                    cell = rowx.createCell(i);
                    cell.setCellValue(data);
                }
                bb++;
                yh++;
            }

            rs.close();
            stat.close();
            // conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Untuk Komponen Yang Salah (warna Merah)
     */
    public void showDialog(JDialog dialog, int wt, int ht) {
        java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        //  int wt = 940;
        //  int ht = 630;
        int w = screen.width;
        int h = screen.height;
        dialog.setLocation((w - wt) / 2, (h - ht) / 2);
        dialog.setSize(wt, ht);
        dialog.setVisible(true);
    }

    public void hapusTabel(JTable table) {
        while (table.getRowCount() > 0) {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }

    }

    public String getBulan(int month) {
        String[] monthNames = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        return monthNames[month];
    }

    public String getBulanTanggal(String b) {
        String tahun = b.substring(0, 4);
        String bln = b.substring(5, 7);
        String tg = b.substring(8, 10);
        int month = Integer.parseInt(bln);
        String[] monthNames = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        return tg + " " + monthNames[month] + " " + tahun;
    }

    public boolean kirimPesan(Connection kon, String tujuan, int id_pihak, String pesan, String methode, String judul) {
        boolean ok = false;
        String sql = "INSERT INTO musik_pesan(tujuan, id_pihak, pesan, methode, from_date, status, judul) VALUES (?,?,?,?,now(),'BELUM',?)";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            stat.setString(1, tujuan);
            stat.setInt(2, id_pihak);
            stat.setString(3, pesan);
            stat.setString(4, methode);
            stat.setString(5, judul);
            stat.executeUpdate();
            ok = true;
        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ok;
    }

    public void hapusDataKomponen(JPanel panel) {
        for (Component C : panel.getComponents()) {
            if (C instanceof JTextField) {
                ((JTextComponent) C).setText(""); //abstract superclass
            }
            if (C instanceof JTextArea) {
                ((JTextArea) C).setText("");
            }
            if (C instanceof JComboBox) {
                ((JComboBox) C).setSelectedIndex(0); //abstract superclass
            }
        }
    }

    public boolean updateData(Connection kon, String tabel, String[][] data, String filter) {
        boolean sukses = false;
        try {
            int y = data.length;
            String sKolom = "";
            //  String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "\'");
                sKolom = sKolom + kolom + " = '" + isi + "',";
                //   sIsi = sIsi + "'" + isi + "',";
            }

            String sql = "UPDATE " + tabel
                    + " SET " + sKolom.substring(0, sKolom.length() - 1)
                    + " WHERE " + filter;

            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;
        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public boolean simpanData(Connection kon, String tabel, String[][] data, boolean from) {
        boolean sukses = false;
        String hSQL = "";
        try {
            int y = data.length;
            String sKolom = "";
            String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "''");
                isi = isi.replaceAll("\'", "''");
                sKolom = sKolom + kolom + ",";
                sIsi = sIsi + "'" + isi + "',";
            }
            if (from) {
                sKolom = sKolom + "from_date ";
                sIsi = sIsi + "now() ";
            }
            String sql = "INSERT INTO " + tabel
                    + " (" + sKolom.substring(0, sKolom.length() - 1)
                    + ") VALUES (" + sIsi.substring(0, sIsi.length() - 1) + ")";
            hSQL = sql;
            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;
        } catch (SQLException ex) {
            new penangananDialog().pesanError("GAGAL SIMPAN ORDER", ex.getMessage(), hSQL);
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public int simpanDataReturn(Connection kon, String tabel, String[][] data, boolean from) {
        int sukses = 0;
        String hSQL = "";
        try {
            int y = data.length;
            String sKolom = "";
            String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "''");
                isi = isi.replaceAll("\'", "''");
                sKolom = sKolom + kolom + ",";
                sIsi = sIsi + "'" + isi + "',";
            }
            if (from) {
                sKolom = sKolom + "from_date ";
                sIsi = sIsi + "now() ";
            }
            String sql = "INSERT INTO " + tabel
                    + " (" + sKolom.substring(0, sKolom.length() - 1)
                    + ") VALUES (" + sIsi.substring(0, sIsi.length() - 1) + ")";
            hSQL = sql;
            PreparedStatement stat = kon.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            stat.executeUpdate();
            ResultSet rs = stat.getGeneratedKeys();
            if (rs.next()) {
                sukses = rs.getInt(1);
            }

        } catch (SQLException ex) {
            new penangananDialog().pesanError("GAGAL SIMPAN ORDER", ex.getMessage(), hSQL);
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public void closeDialogWarning(JDialog dialog) {
        int y = JOptionPane.showConfirmDialog(dialog, "Anda yakin ingin menutup dialog ini?", "Tutup Dialog", JOptionPane.YES_NO_OPTION);
        if (y == JOptionPane.YES_OPTION) {
            dialog.dispose();
        }

    }

    public void closeDialogSukses(JDialog dialog) {
        dialog.dispose();
    }

    public synchronized String getStringSQL(Connection kon, String sql) {
        String hasil = "";
        //  System.out.println(sql);
        try {
            //  Connection kon = getKoneksi();

            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            if (rset.next()) {
                hasil = rset.getString(1);
                if (hasil == null) {
                    hasil = "";
                }
            }
            rset.close();
            stat.close();
            // kon.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hasil;
    }

    public synchronized int getIntSQL(Connection kon, String sql) {
        int hasil = 0;
        try {
            //  Connection kon = getKoneksi();

            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            if (rset.next()) {
                hasil = rset.getInt(1);

            }
            rset.close();
            stat.close();
            // kon.close();
        } catch (SQLException ex) {
            //  Logger.getLogger(utamaSurat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    public synchronized HashMap<String, String> setHashMap(Connection kon, String query) {
        HashMap<String, String> hmap = new HashMap<>();
        //    hmap.put("KOSONGKOK", "KOSONGKOK");
        //    System.out.println("HASH MAP :"+query);
        try {
            //  Connection kon = getKoneksi();

            PreparedStatement stat = kon.prepareStatement(query);
            ResultSet rset = stat.executeQuery();
            //  int x = 0;
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                hmap.put(a, b);
                //           System.out.println("MAP : "+a+" - "+b);
                //    x++;
            }
            rset.close();
            stat.close();
            // kon.close();
        } catch (SQLException ex) {
            System.out.println("ERROR : " + ex.getMessage());
        }
        return hmap;
    }

    public synchronized Object[] setDataEdit(Connection kon, String query) {
        Object[] data = null;

        try {

            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(query);

            int columns = rs.getMetaData().getColumnCount();
            if (rs.next()) {
                data = new Object[columns + 1];

                for (int i = 1; i <= columns; i++) {
                    Object bj = rs.getObject(i);
                    if (bj == null) {
                        bj = "";
                    }
                    data[i - 1] = bj;
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR ITU : " + query);
        }
        return data;
    }

    public boolean setSQL(Connection kon, String sql) {
        boolean sukses = false;

        //   System.out.println(sql);
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;

            if (sql.startsWith("DELETE") || sql.startsWith("delete") || sql.startsWith("UPDATE") || sql.startsWith("update")) {
                //   simpanDataLog(kon, sql);
            }
        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;

    }

    public void simpanDataLog(Connection kon, String data) {
        try {
            String sql = "INSERT INTO musik_data(dataku, tgl) VALUES (?, now())";
            PreparedStatement stat = kon.prepareStatement(sql);
            stat.setString(1, data);
            stat.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setDataTabel(Connection kon, JTable table, String Query, int kol) {
        try {
            ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
            table.getTableHeader().setFont(new java.awt.Font("Tahoma", Font.BOLD, 12));
            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            //To remove previously added rows
            while (table.getRowCount() > 0) {
                ((DefaultTableModel) table.getModel()).removeRow(0);
            }

            int columns = rs.getMetaData().getColumnCount();
            //  System.out.println(columns);
            int yh = 1;
            while (rs.next()) {
                Object[] row = new Object[columns + 1];
                row[0] = yh;
                for (int i = 1; i <= columns; i++) {

                    Object ds = rs.getObject(i);
                    if (ds == null) {
                        ds = "";
                    }
                    row[i] = ds;
                }

                ((DefaultTableModel) table.getModel()).insertRow(rs.getRow() - 1, row);

                yh++;
            }

            table.setDefaultRenderer(Object.class, new EvenOddRenderer());

            //  ColumnsAutoSizer.sizeColumnsToFit(table);
            table.getColumnModel().getColumn(kol).setMinWidth(0);
            table.getColumnModel().getColumn(kol).setMaxWidth(0);
            table.getColumnModel().getColumn(kol).setWidth(0);

            DefaultTableCellRenderer no = new DefaultTableCellRenderer() {

                public void setBackground(Color v) {

                    super.setBackground(java.awt.SystemColor.activeCaption);
                    //   super.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
                }
            };

            table.getColumnModel().getColumn(0).setCellRenderer(no);
            no.setHorizontalAlignment(JLabel.CENTER);
            rs.close();
            stat.close();
            // conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setDataTabelCek(Connection kon, JTable table, String Query, int kol, int cek) {
        try {
            ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
            table.getTableHeader().setFont(new java.awt.Font("Tahoma", Font.BOLD, 12));
            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            //To remove previously added rows
            while (table.getRowCount() > 0) {
                ((DefaultTableModel) table.getModel()).removeRow(0);
            }

            int columns = rs.getMetaData().getColumnCount();
            //  System.out.println(columns);
            int yh = 1;
            while (rs.next()) {
                Object[] row = new Object[columns + 1];
                row[0] = yh;
                for (int i = 1; i <= columns; i++) {
                    if (i == cek) {
                        row[i] = Boolean.parseBoolean(rs.getObject(i).toString());
                    } else {
                        Object ds = rs.getObject(i);
                        if (ds == null) {
                            ds = "";
                        }
                        row[i] = ds;
                    }

                }

                ((DefaultTableModel) table.getModel()).insertRow(rs.getRow() - 1, row);

                yh++;
            }

            table.setDefaultRenderer(Object.class, new EvenOddRenderer());

            //    ColumnsAutoSizer.sizeColumnsToFit(table);
            table.getColumnModel().getColumn(kol).setMinWidth(0);
            table.getColumnModel().getColumn(kol).setMaxWidth(0);
            table.getColumnModel().getColumn(kol).setWidth(0);

            DefaultTableCellRenderer no = new DefaultTableCellRenderer() {

                public void setBackground(Color v) {

                    super.setBackground(java.awt.SystemColor.activeCaption);
                    //   super.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
                }
            };

            table.getColumnModel().getColumn(0).setCellRenderer(no);
            no.setHorizontalAlignment(JLabel.CENTER);
            rs.close();
            stat.close();
            // conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getStringFilter(String[] aku, String f) {
        String a = "";
        int y = aku.length;
        for (int x = 0; x < y; x++) {
            String kolom = aku[x].toString();
            a = a + "OR " + kolom + " LIKE '%" + f + "%' ";
        }
        a = a.replaceFirst("OR", "WHERE (");
        a = a + ") ";
        return a;
    }

    public String ribuan(String uang) {
        long money = 0;
        try {
            money = Long.parseLong(uang);
        } catch (Exception e) {
            money = 0;
        }
        String format = NumberFormat.getNumberInstance(Locale.ENGLISH).format(money);
        //     StringTokenizer token = new StringTokenizer(format, ".");
        //    format = token.nextToken();
        //    String fraction = (token.hasMoreTokens()) ? token.nextToken() : "-";
        format = format.replace(',', '.');

        return format;
    }

    public String ribuan(long money) {

        String format = NumberFormat.getNumberInstance(Locale.ENGLISH).format(money);
        //     StringTokenizer token = new StringTokenizer(format, ".");
        //    format = token.nextToken();
        //    String fraction = (token.hasMoreTokens()) ? token.nextToken() : "-";
        format = format.replace(',', '.');

        return format;
    }

    public void setJComboBoxList(JComboBox box, String tipe, String pilih, Connection kon) {
        //   getConnection u = new getConnection();
        //   Connection kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        try {

            box.removeAllItems();

        } catch (Exception ex) {

        }
        box.addItem(pilih);
        try {
            PreparedStatement stat = kon.prepareStatement("SELECT id, nama FROM musik_master_list WHERE tipe = ? AND thru_date is null ORDER BY urut");
            stat.setString(1, tipe);
            ResultSet rset = stat.executeQuery();

            Vector model = new Vector();
            while (rset.next()) {
                String nama = rset.getString("nama");
                // int id = rset.getInt("id");
                box.addItem(nama);

            }

        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setJComboBoxListQuery(JComboBox box, String tipe, String pilih, Connection kon) {
        //   getConnection u = new getConnection();
        //   Connection kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        try {

            box.removeAllItems();

        } catch (Exception ex) {

        }
        box.addItem(pilih);
        try {
            PreparedStatement stat = kon.prepareStatement(tipe);
            //  stat.setString(1, tipe);
            ResultSet rset = stat.executeQuery();

            //    Vector model = new Vector();
            while (rset.next()) {
                String nama = rset.getString(1);
                // int id = rset.getInt("id");
                box.addItem(nama);

            }

        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setJComboBoxListVek(JComboBox box, String tipe, String pilih, Connection kon) {
        //   getConnection u = new getConnection();
        //   Connection kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        try {

            box.removeAllItems();

        } catch (Exception ex) {

        }
        box.addItem(new Item<String>("0", pilih));
        try {
            PreparedStatement stat = kon.prepareStatement("SELECT id, nama FROM musik_master_list WHERE tipe = ? AND thru_date IS NULL ORDER BY urut");
            stat.setString(1, tipe);
            ResultSet rset = stat.executeQuery();

            while (rset.next()) {
                String id = rset.getString(1);
                String nama = rset.getString(2);
                box.addItem(new Item<String>(id, nama));

            }

        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setJComboBoxListVektor(JComboBox box, String sql, String pilih, Connection kon) {
        //   getConnection u = new getConnection();
        //   Connection kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        //   System.out.println(sql);
        try {

            box.removeAllItems();

        } catch (Exception ex) {

        }
        box.addItem(new Item<String>("0", pilih));
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            // stat.setString(1, tipe);
            ResultSet rset = stat.executeQuery();

            while (rset.next()) {
                String id = rset.getString(1);
                String nama = rset.getString(2);
                box.addItem(new Item<String>(id, nama));

            }

        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setJListVektor(JList box, String sql, String pilih, Connection kon) {
        //   getConnection u = new getConnection();
        //   Connection kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        box.removeAll();
        // box.addItem(new Item<String>("0", pilih));
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            // stat.setString(1, tipe);
            ResultSet rset = stat.executeQuery();

            Vector v = new Vector();
            while (rset.next()) {
                String id = rset.getString(1);
                String nama = rset.getString(2);
                v.addElement(nama);

            }
            box.setListData(v);
        } catch (SQLException ex) {
            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setJTextFieldSalah(JTextField text) {
        text.setBackground(Color.red);
        text.requestFocus();
        text.selectAll();
    }

    public void setJTextAreaSalah(JTextArea text) {
        text.setBackground(Color.red);
        text.requestFocus();
        text.selectAll();
    }

    /**
     * Untuk Komponen Yang Pilih (warna Kuning)
     */
    public void setJTextFieldPilih(JTextField text) {
        text.setBackground(Color.yellow);
        text.requestFocus();
        text.selectAll();
    }

    public void setJTextAreaPilih(JTextArea text) {
        text.setBackground(Color.yellow);
        text.requestFocus();
        text.selectAll();
    }

    /**
     * Untuk Komponen Yang Aktif (warna kuning)
     */
    public void setJTextFieldPilihSaja(JTextField text) {
        text.setBackground(Color.yellow);
    }

    /**
     * Untuk Komponen Yang Benar (warna Putih)
     */
    public void setJTextFieldBenar(JTextField text) {
        text.setBackground(Color.white);
    }

    public void setJTextAreaBenar(JTextArea text) {
        text.setBackground(Color.white);
    }

    /**
     * Untuk Komponen Yang Tidak Aktif (warna Cokelat muda)
     */
    public void setJTextFieldTidakAktif(JTextField text) {
        text.setEnabled(false);
        text.setBackground(new Color(236, 233, 216));
    }

    /**
     * Untuk Komponen Yang Aktif (warna putih)
     */
    public void setJTextFieldAktif(JTextField text) {
        text.setEnabled(true);
        text.setBackground(Color.WHITE);
    }

    public void setJComboBoxSalah(JComboBox cBox) {
        cBox.setBackground(Color.red);
        cBox.requestFocus();
        cBox.showPopup();
    }

    /**
     * Untuk KeyPressed komponen yang harus ada data (NOT NULL)
     *
     * @param evt
     * @param textThis
     * @param textNext
     */
    public void setJTextHarusIsi(KeyEvent evt, JTextField textThis, JTextField textNext) {
        int x = textThis.getText().length();
        if (!(x == 0)) {
            setJTextFieldPilihSaja(textThis);
            if (evt.getKeyCode() == evt.VK_ENTER) {
                setJTextFieldBenar(textThis);
                setJTextFieldPilih(textNext);
            }
        } else {
            setJTextFieldSalah(textThis);
        }
    }

    public void setJTextHarusIsiTextToArea(KeyEvent evt, JTextField textThis, JTextArea textNext) {
        int x = textThis.getText().length();
        if (!(x == 0)) {
            setJTextFieldPilihSaja(textThis);
            if (evt.getKeyCode() == evt.VK_ENTER) {
                setJTextFieldBenar(textThis);
                setJTextAreaPilih(textNext);
            }
        } else {
            setJTextFieldSalah(textThis);
        }
    }

    public void setJTextHarusIsiTextToButton(KeyEvent evt, JTextField textThis, JButton textNext) {
        int x = textThis.getText().length();
        if (!(x == 0)) {
            setJTextFieldPilihSaja(textThis);
            if (evt.getKeyCode() == evt.VK_ENTER) {
                setJTextFieldBenar(textThis);
                textNext.requestFocus();
            }
        } else {
            setJTextFieldSalah(textThis);
        }
    }

    /**
     * Untuk KeyPressed komponen yang tidak harus ada data (ALLOW NULL)
     *
     * @param evt
     * @param textThis
     * @param textNext
     */
    public void setJTextTidakHarusIsi(KeyEvent evt, JTextField textThis, JTextField textNext) {
        setJTextFieldPilihSaja(textThis);
        if (evt.getKeyCode() == evt.VK_ENTER) {
            setJTextFieldBenar(textThis);
            setJTextFieldPilih(textNext);
        }
    }

    public void setJTextValidasi(JTextField text) {
        if (text.getText().trim().isEmpty()) {
            text.setBackground(Color.red);
        } else {
            setJTextFieldBenar(text);
        }
    }

    class EvenOddRenderer implements TableCellRenderer {

        public final DefaultTableCellRenderer DEFAULT_RENDERER
                = new DefaultTableCellRenderer();

        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {
            Component renderer
                    = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                            isSelected, hasFocus, row, column);
            Color foreground, background;
            if (isSelected) {
                foreground = Color.BLUE;
                background = Color.PINK;//java.awt.SystemColor.activeCaption;
            } else if (row % 2 == 0) {
                foreground = Color.black;
                background = Color.WHITE;
            } else {
                foreground = Color.black;
                background = new java.awt.Color(218, 218, 218);//java.awt.SystemColor.activeCaption;
            }
            renderer.setForeground(foreground);
            renderer.setBackground(background);
            return renderer;
        }
    }
}
