/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author iweks
 */
public class ClipMover extends MouseInputAdapter {

    panelFoto cropping;
    Point offset;
    boolean dragging;
    JLabel panel;
    int xx = 5;

    public ClipMover(panelFoto c, JLabel p) {
        cropping = c;
        panel = p;
        offset = new Point();
        dragging = false;

    }

    public void setZoom(int xy) {
        this.xx = xy;
    //    System.out.println(x);
        //  clip = new Rectangle(60 * zoom, 80 * zoom);
        // clip.setLocation(xx, yy);
        dragging = false;
        BufferedImage clipped = cropping.clipImage();
        panel.setSize(60 * xx, 80 * xx);
        panel.setIcon(new ImageIcon(clipped));
    }

    public void mousePressed(MouseEvent e) {
        Point p = e.getPoint();
        if (cropping.clip.contains(p)) {
            offset.x = p.x - cropping.clip.x;
            offset.y = p.y - cropping.clip.y;
            dragging = true;
        }
    }

    public void mouseReleased(MouseEvent e) {
        dragging = false;
        BufferedImage clipped = cropping.clipImage();
        panel.setSize(60 * xx, 80 * xx);
        panel.setIcon(new ImageIcon(clipped));
    }

    public void mouseDragged(MouseEvent e) {
        if (dragging) {
            int x = e.getX() - offset.x;
            int y = e.getY() - offset.y;
            cropping.setClip(x, y);
        }
    }
}
