/*
 * getWaktu.java
 *
 * Created on 21 Juli 2008, 21:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package utility;

import java.util.Calendar;

/**
 *
 * @author Rara Kaltsum
 */
public class getWaktu {
    private String tanggal = "";
    private String bulan = "";
    private String tahun = "";
    private String jam = "";
    private String menit = "";
    private String detik = "";
    
    /** Creates a new instance of getWaktu */
    public getWaktu() {
        String tgl_nol = "";
        String bln_nol = "";
        // Mengambil Waktu sekarang
        Calendar dt = Calendar.getInstance();
        int tgl = dt.get(Calendar.DAY_OF_MONTH);
        int bln = dt.get(Calendar.MONTH)+1;
        int thn = dt.get(Calendar.YEAR);
        
        // Apabila nilai tanggal lebih kecil dari 10
        if (tgl < 10) {
            tgl_nol = "0";
        }
        if (bln < 10) {
            bln_nol = "0";
        }
        tanggal = tgl_nol + Integer.toString(tgl);
        bulan = bln_nol + Integer.toString(bln);
        tahun = Integer.toString(thn);
        
//======================================================================
        String nol_jam = "";
        String nol_menit = "";
        String nol_detik = "";
        
        // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
        int nilai_jam = dt.get(Calendar.HOUR_OF_DAY);
        int nilai_menit = dt.get(Calendar.MINUTE);
        int nilai_detik = dt.get(Calendar.SECOND);
        
        // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
        if (nilai_jam <= 9) {
            // Tambahkan "0" didepannya
            nol_jam = "0";
        }
        // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
        if (nilai_menit <= 9) {
            // Tambahkan "0" didepannya
            nol_menit = "0";
        }
        // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
        if (nilai_detik <= 9) {
            // Tambahkan "0" didepannya
            nol_detik = "0";
        }
        
        // Membuat String JAM, MENIT, DETIK
        jam = nol_jam + Integer.toString(nilai_jam);
        menit = nol_menit + Integer.toString(nilai_menit);
        detik = nol_detik + Integer.toString(nilai_detik);  
    }
    
    public String getTanggal(){
        return tanggal;
    }
    
    public String getBulan(){
        return bulan;
    }
    
    public String getTahun(){
        return tahun;
    }
    
    public String getJam(){
        return jam;
    }
    
    public String getMenit(){
        return menit;
    }
    
    public String getDetik(){
        return detik;
    }
    
    public String getWaktu(){
        String date = tanggal+"/"+bulan+"/"+tahun+"  "+jam+":"+menit+":"+detik;
        return date;
    }
    public String getWaktuJam(){
        String date = jam+":"+menit+":"+detik;
        return date;
    }
    public String getWaktuTanggal(){
        String date = tanggal+"/"+bulan+"/"+tahun;
        return date;
    }
    public String getWaktuTanggalSQL(){
        String date = tahun+"-"+bulan+"-"+tanggal;
        return date;
    }
    public int getSortTanggal(){
        int rt = Integer.parseInt(tahun+bulan+tanggal);
        return rt;
    }
    
    public int getSortJam(){
        int rt = Integer.parseInt(jam+menit+detik);
        return rt;
    }
}
