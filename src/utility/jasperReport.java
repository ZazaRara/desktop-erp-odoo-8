/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.sql.Connection;
import java.util.Hashtable;
import java.util.List;
import javax.swing.JDialog;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author iweks24
 */
public class jasperReport {

    public jasperReport() {

    }

    public void tampilReport(JasperPrint JPrint) {
        JasperViewer jasperViewer = new JasperViewer(JPrint, false);
        JDialog dialog = new JDialog();//the owner
        dialog.setContentPane(jasperViewer.getContentPane());
        dialog.setSize(jasperViewer.getSize());
        // dialog.setTitle("XXXXX");
        dialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setModal(true);
        dialog.setVisible(true);
    }

    public JasperPrint getReport(String source, Hashtable hparam, Connection kon, boolean tam) {
        JasperPrint JPrint = null;
        String folder = System.getProperty("user.dir") + "\\Laporan\\";
        try {

            JasperReport report = (JasperReport) JRLoader.loadObject(folder+source+".jasper");
            JPrint = JasperFillManager.fillReport(report, hparam, kon);
            if(tam){
                tampilReport(JPrint);
            }
            //     JasperViewer.viewReport(JPrint, false);

        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        }
        return JPrint;
    }

    public JasperPrint multipageLinking(JasperPrint page1, JasperPrint page2) {
        List<JRPrintPage> pages = page2.getPages();
        for (int count = 0; count
                < pages.size(); count++) {
            page1.addPage(pages.get(count));
        }

        return page1;
    }
}
