
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/*
 * pengaturan.java
 *
 * Created on 19 September 2010, 21:39
 */
/**
 *
 * @author Iweks
 */
public class pengaturan extends javax.swing.JFrame {

    String folder;
    static String secretKey = "iweksgantengsekali";
    private static final String ALGO = "AES";
    private static final byte[] keyValue
            = new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't',
                'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};

    /**
     * Creates new form pengaturan
     */
    public pengaturan() {
        initComponents();
        folder = System.getProperty("user.dir");
        cekDatabase();
        setVisible(true);
    }

    private static String encrypt(String Data) {
        String hasil = "";
        try {
            Key key = new SecretKeySpec(keyValue, ALGO);
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = c.doFinal(Data.getBytes());
            String encryptedValue = new BASE64Encoder().encode(encVal);
            hasil = encryptedValue;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    private static String decrypt(String encryptedData) {
        String hasil = "";
        try {
            Key key = new SecretKeySpec(keyValue, ALGO);
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            hasil = decryptedValue;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(pengaturan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    private boolean cekDatabase() {
        t_cek.setText("");
        b_db.setEnabled(false);
        Properties prop = new Properties();
        InputStream input = null;
        boolean ok = false;
        try {
            input = new FileInputStream(folder + "\\Laporan\\config.properties");

            // load a properties file
            prop.load(input);
            String jdbc = prop.getProperty("driver");
            //  String url = prop.getProperty("url");
            String user = prop.getProperty("user");
            String pass = prop.getProperty("pass");
            String ip = prop.getProperty("ip");
            String port = prop.getProperty("port");
            String nama = prop.getProperty("nama");

            t_driver.setSelectedItem(decrypt(jdbc));
            t_user.setText(decrypt(user));
            t_pass.setText(decrypt(pass));
            t_ip.setText(decrypt(ip));
            t_port.setText(decrypt(port));
            t_nama.setText(decrypt(nama));
            ok = true;

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return ok;
    }

    private boolean cekKoneksi() {
        boolean ok = false;

        String jdbc = t_driver.getSelectedItem().toString();
        //  String url = t_url.getText();
        String user = t_user.getText();
        String pass = t_pass.getText();
        String ip = t_ip.getText();
        String port = t_port.getText();
        String nama = t_nama.getText();
        String url = "jdbc:postgresql://" + ip + ":" + port + "/" + nama;
        if (jdbc.equalsIgnoreCase("com.microsoft.sqlserver.jdbc.SQLServerDriver")) {
            //  "jdbc:sqlserver://192.168.10.2:1433;DatabaseName=ARISTA_VERSI_3.1";
            url = "jdbc:sqlserver://" + ip + ":" + port + ";DatabaseName=" + nama;
        } else if (jdbc.equalsIgnoreCase("com.mysql.jdbc.Driver")) {
            //  "jdbc:sqlserver://192.168.10.2:1433;DatabaseName=ARISTA_VERSI_3.1";
            url = "jdbc:mysql://" + ip + ":" + port + "/" + nama;
        }

        t_url.setText(url);

        Connection kk = getConnection(jdbc, url, user, pass);
        if (kk == null) {
            ok = false;
        } else {
            ok = true;
        }
        return ok;
    }

    private boolean simpanDB() {
        boolean ok = false;
        String jdbc = t_driver.getSelectedItem().toString();
        String url = t_url.getText();
        String user = t_user.getText();
        String pass = t_pass.getText();
        String ip = t_ip.getText();
        String port = t_port.getText();
        String nama = t_nama.getText();

        Properties prop = new Properties();
        OutputStream output = null;

        try {

            output = new FileOutputStream(folder + "\\Laporan\\config.properties");

            // set the properties value
          //  System.out.println(encrypt(jdbc));
            prop.setProperty("driver", encrypt(jdbc));
            prop.setProperty("user", encrypt(user));
            prop.setProperty("pass", encrypt(pass));
            prop.setProperty("ip", encrypt(ip));
            prop.setProperty("port", encrypt(port));
            prop.setProperty("nama", encrypt(nama));
            prop.setProperty("url", encrypt(url));
            // save properties to project root folder
            prop.store(output, null);
            ok = true;
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return ok;
    }

    public Connection getConnection(String driverJDBC, String url, String user, String password) {
        boolean driver = false;
        Connection koneksi = null;
        try {
            // Penyambungan Driver
            Class.forName(driverJDBC);
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            koneksi = null;
            //  JOptionPane.showMessageDialog(null, "Fail Loading Driver : "
            //          +driverJDBC+"\nDetail : "+not.getMessage(),"Error Database", JOptionPane.OK_OPTION);
            System.out.println(not.getMessage());
            //  System.exit(0);
        }

        if (driver == true) {

            try {
                koneksi = java.sql.DriverManager.getConnection(url, user, password);

            } // Akhir try
            catch (java.sql.SQLException sqln) {
                koneksi = null;
                System.out.println("Fail connecting with Database Server..."
                        + "\nDetail : " + sqln.getMessage());
                //     JOptionPane.showMessageDialog(null, "Fail connecting with Database Server..."
                //            +"\nDetail : "+sqln.getMessage(),"Error Database", JOptionPane.OK_OPTION);
                //    System.exit(0);

            }
        }

        return koneksi;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        data = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        t_driver = new javax.swing.JComboBox();
        t_ip = new javax.swing.JTextField();
        t_port = new javax.swing.JTextField();
        t_nama = new javax.swing.JTextField();
        t_user = new javax.swing.JTextField();
        b_test = new javax.swing.JButton();
        t_cek = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        t_url = new javax.swing.JTextField();
        t_pass = new javax.swing.JPasswordField();
        jPanel6 = new javax.swing.JPanel();
        b_db = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Pengaturan Database");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().add(jPanel2, java.awt.BorderLayout.NORTH);
        getContentPane().add(jPanel3, java.awt.BorderLayout.SOUTH);
        getContentPane().add(jPanel4, java.awt.BorderLayout.EAST);
        getContentPane().add(jPanel5, java.awt.BorderLayout.WEST);

        data.setBackground(new java.awt.Color(255, 255, 255));
        data.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Driver JDBC");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("IP Server");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 90, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Port");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Nama DB");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 90, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("User");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 90, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Password");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 90, 20));

        t_driver.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "com.mysql.jdbc.Driver", "org.postgresql.Driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver" }));
        t_driver.setSelectedIndex(1);
        jPanel1.add(t_driver, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 150, -1));
        jPanel1.add(t_ip, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 150, -1));
        jPanel1.add(t_port, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 50, -1));
        jPanel1.add(t_nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 150, -1));
        jPanel1.add(t_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, 150, -1));

        b_test.setText("Test Koneksi");
        b_test.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_testActionPerformed(evt);
            }
        });
        jPanel1.add(b_test, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, -1, -1));

        t_cek.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        t_cek.setText("jLabel7");
        jPanel1.add(t_cek, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 190, 180, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText(":");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 20, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText(":");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 20, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText(":");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 20, 20));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText(":");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 20, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText(":");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 20, 20));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText(":");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, 20, 20));
        jPanel1.add(t_url, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, 0, 0));
        jPanel1.add(t_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 150, 20));

        data.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        b_db.setText("Simpan Pengaturan");
        b_db.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_dbActionPerformed(evt);
            }
        });
        jPanel6.add(b_db);

        data.add(jPanel6, java.awt.BorderLayout.SOUTH);

        jTabbedPane1.addTab("Database", data);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(590, 556));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    private void b_dbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_dbActionPerformed
        if (simpanDB()) {
            cekDatabase();
            t_cek.setText("Sukses Menyimpan");
            t_cek.setForeground(Color.BLUE);
            JOptionPane.showMessageDialog(this, "Sukses Menyimpan Pengaturan Database");
            this.dispose();
            new awal();
        } else {
            JOptionPane.showMessageDialog(this, "GAGAL MENYIMPAN KONFIGURASI DATABASE");
            t_cek.setText("GAGAL Menyimpan");
            t_cek.setForeground(Color.RED);
            b_db.setEnabled(false);
        }
    }//GEN-LAST:event_b_dbActionPerformed

    private void b_testActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_testActionPerformed
        if (cekKoneksi()) {
            t_cek.setText("Koneksi Sukses");
            t_cek.setForeground(Color.BLUE);
            b_db.setEnabled(true);
        } else {
            t_cek.setText("Koneksi Gagal");
            t_cek.setForeground(Color.RED);
            b_db.setEnabled(false);
        }
    }//GEN-LAST:event_b_testActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_db;
    private javax.swing.JButton b_test;
    private javax.swing.JPanel data;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel t_cek;
    private javax.swing.JComboBox t_driver;
    private javax.swing.JTextField t_ip;
    private javax.swing.JTextField t_nama;
    private javax.swing.JPasswordField t_pass;
    private javax.swing.JTextField t_port;
    private javax.swing.JTextField t_url;
    private javax.swing.JTextField t_user;
    // End of variables declaration//GEN-END:variables

    public class MyComboBoxEditor extends DefaultCellEditor {

        public MyComboBoxEditor(String[] items) {
            super(new JComboBox(items));
        }
    }
}
