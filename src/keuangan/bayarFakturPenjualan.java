/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keuangan;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import net.sf.jasperreports.engine.JasperPrint;
import utility.Item;
import utility.jasperReport;
import utility.penangananDialog;
import utility.penangananKomponen;
import utility.saring_karakter;

/**
 *
 * @author iweks24
 */
public class bayarFakturPenjualan extends javax.swing.JInternalFrame {

    Connection kon;
    String user;
    penangananKomponen komp = new penangananKomponen();
    penangananDialog pesan = new penangananDialog();
    jasperReport jr;
    int rowCek = -1;
    int totRow = 0;
    double batasDiskon = 5;
    boolean klik = false;

    /**
     * Creates new form cetakFaktur
     */
    public bayarFakturPenjualan(String user, Connection kon) {
        this.kon = kon;
        this.user = user;
        jr = new jasperReport();
        initComponents();
        komp.setJComboBoxListVektor(sales, "SELECT a.id, UPPER(b.name) as nama FROM res_users a "
                + "INNER JOIN res_partner b ON b.id = a.partner_id WHERE a.active = 'true'", "-- SEMUA SALES --", kon);

        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        showData();

        //       komp.setJComboBoxListVektor(dPembayaran, "SELECT id, name FROM account_payment_term ORDER BY id", "-- Pilih Tempo --", kon);
        setTabel();
    }

    private void setTabel() {

        tgl_mulai.setVisible(false);
        tgl_sampai.setVisible(false);
        labelTgl.setVisible(false);
        labelFak.setVisible(false);
        ((DefaultTableCellRenderer) tabelFaktur.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        tabelFaktur.getTableHeader().setFont(new java.awt.Font("Tahoma", Font.BOLD, 12));

        //   tabelFaktur.setDefaultRenderer(Object.class, new EvenOddRenderer());
        DefaultTableCellRenderer no = new DefaultTableCellRenderer() {

        };

        tabelFaktur.getColumnModel().getColumn(1).setMaxWidth(60);
        tabelFaktur.getColumnModel().getColumn(1).setWidth(60);
        tabelFaktur.getColumnModel().getColumn(1).setMinWidth(60);
        tabelFaktur.getColumnModel().getColumn(2).setMaxWidth(60);
        tabelFaktur.getColumnModel().getColumn(2).setWidth(60);
        tabelFaktur.getColumnModel().getColumn(2).setMinWidth(60);

        tabelFaktur.getColumnModel().getColumn(5).setMaxWidth(120);
        tabelFaktur.getColumnModel().getColumn(5).setWidth(120);
        tabelFaktur.getColumnModel().getColumn(5).setMinWidth(120);

        tabelFaktur.getColumnModel().getColumn(4).setMaxWidth(60);
        tabelFaktur.getColumnModel().getColumn(4).setWidth(60);
        tabelFaktur.getColumnModel().getColumn(4).setMinWidth(60);

        tabelFaktur.getColumnModel().getColumn(6).setMaxWidth(80);
        tabelFaktur.getColumnModel().getColumn(6).setWidth(80);
        tabelFaktur.getColumnModel().getColumn(6).setMinWidth(80);

        tabelFaktur.getColumnModel().getColumn(7).setMaxWidth(120);
        tabelFaktur.getColumnModel().getColumn(7).setWidth(120);
        tabelFaktur.getColumnModel().getColumn(7).setMinWidth(120);

        /**
         * tabelBarang.getColumnModel().getColumn(7).setMaxWidth(0);
         * tabelBarang.getColumnModel().getColumn(7).setWidth(0);
         * tabelBarang.getColumnModel().getColumn(7).setMinWidth(0);
         *
         * tabelBarang.getColumnModel().getColumn(8).setMaxWidth(0);
         * tabelBarang.getColumnModel().getColumn(8).setWidth(0);
         * tabelBarang.getColumnModel().getColumn(8).setMinWidth(0);
         */
        tabelFaktur.getColumnModel().getColumn(0).setMaxWidth(50);
        tabelFaktur.getColumnModel().getColumn(0).setWidth(40);
        tabelFaktur.getColumnModel().getColumn(0).setMinWidth(40);
        //      tabelFaktur.setRowHeight(30);

        tabelFaktur.getColumnModel().getColumn(0).setCellRenderer(no);
        tabelFaktur.getColumnModel().getColumn(2).setCellRenderer(no);
        tabelFaktur.getColumnModel().getColumn(4).setCellRenderer(no);
        tabelFaktur.getColumnModel().getColumn(5).setCellRenderer(no);
        tabelFaktur.getColumnModel().getColumn(6).setCellRenderer(no);
        tabelFaktur.getColumnModel().getColumn(7).setCellRenderer(no);

        no.setHorizontalAlignment(JLabel.CENTER);

        tabelData.getColumnModel().getColumn(0).setMaxWidth(50);
        tabelData.getColumnModel().getColumn(0).setWidth(40);
        tabelData.getColumnModel().getColumn(0).setMinWidth(40);

        tabelData.getColumnModel().getColumn(2).setMaxWidth(0);
        tabelData.getColumnModel().getColumn(2).setWidth(0);
        tabelData.getColumnModel().getColumn(2).setMinWidth(0);

        tabelData.getColumnModel().getColumn(3).setMaxWidth(120);
        tabelData.getColumnModel().getColumn(3).setWidth(120);
        tabelData.getColumnModel().getColumn(3).setMinWidth(120);
        tabelData.getColumnModel().getColumn(4).setMaxWidth(80);
        tabelData.getColumnModel().getColumn(4).setWidth(80);
        tabelData.getColumnModel().getColumn(4).setMinWidth(80);

        tabelData.getColumnModel().getColumn(6).setMaxWidth(120);
        tabelData.getColumnModel().getColumn(6).setWidth(120);
        tabelData.getColumnModel().getColumn(6).setMinWidth(120);
        tabelData.getColumnModel().getColumn(7).setMaxWidth(140);
        tabelData.getColumnModel().getColumn(7).setWidth(140);
        tabelData.getColumnModel().getColumn(7).setMinWidth(140);
        tabelData.getColumnModel().getColumn(8).setMaxWidth(120);
        tabelData.getColumnModel().getColumn(8).setWidth(120);
        tabelData.getColumnModel().getColumn(8).setMinWidth(120);
        tabelData.getColumnModel().getColumn(9).setMaxWidth(150);
        tabelData.getColumnModel().getColumn(9).setWidth(150);
        tabelData.getColumnModel().getColumn(9).setMinWidth(150);
        tabelData.getColumnModel().getColumn(10).setMaxWidth(60);
        tabelData.getColumnModel().getColumn(10).setWidth(60);
        tabelData.getColumnModel().getColumn(10).setMinWidth(60);
        //   tabelBarang.setAutoResizeMode(1);
        //  ColumnsAutoSizer.sizeColumnsToFit(table);
    }

    private void hitungHarga() {
//        int y = tabelFaktur.getRowCount();
//        double total = 0;
//        for (int x = 0; x < y; x++) {
//            String sub = tabelFaktur.getValueAt(x, 6).toString();
//            // sub = sub.replaceFirst(".",","); 
//            total = total + Double.parseDouble(sub);
//        }
//
//        double ppn = total * 0.10;
//        double dpp = total - ppn;
//        dTotal.setText(String.format("%.2f", total));
//        dPPN.setText(String.format("%.2f", ppn));
//        dDPP.setText(String.format("%.2f", dpp));

        int y = tabelFaktur.getRowCount();
        int hasil = 0;
        for (int i = 0; i < y; i++) {
            //   tabelFaktur.setValueAt(String.valueOf(i + 1), i, 0);
            String harga = tabelFaktur.getValueAt(i, 5).toString();
            String dis = tabelFaktur.getValueAt(i, 6).toString();
            String qty = tabelFaktur.getValueAt(i, 4).toString();
            int hdis = (int) (Double.parseDouble(harga) * (Double.parseDouble(dis) / 100));
            int tot = Integer.parseInt(qty) * (Integer.parseInt(harga) - hdis);
            //  String akhirdiskon = String.valueOf(dis);
            String total = String.valueOf(tot);

            tabelFaktur.setValueAt(komp.ribuan(total), i, 7);
            tabelFaktur.setValueAt(komp.ribuan(harga), i, 5);
            //      String total = tabelFaktur.getValueAt(i, 6).toString();
            total = total.replace(".", "");
            hasil = hasil + Integer.parseInt(total);
        }

        int ppn = (int) (hasil * 0.10);
        int dpp = hasil - ppn;
        dTotal.setText(komp.ribuan(hasil));
        dPPN.setText(komp.ribuan(ppn));
        dDPP.setText(komp.ribuan(dpp));
    }

    private void showData() {
        String awal = new java.sql.Date(tgl_mulai.getDate().getTime()).toString();
        String akhir = new java.sql.Date(tgl_sampai.getDate().getTime()).toString();
        String unitku = ((Item) sales.getSelectedItem()).getValue().toString();
        String fsal = "AND inv.user_id = " + unitku;
        String filter = textFilter.getText();
        if (unitku.equalsIgnoreCase("0")) {
            fsal = "";
        }
        //  String status = "inv.\"state\" = 'open'";
        String sql = "SELECT inv.id,'false' as cek, inv.\"number\", inv.date_invoice, "
                //+ "inv.date_due, \n"
                + "p.\"name\", p.city, ss.name as nama_sales, "
                //  + "inv.amount_untaxed as jml_no ,inv.amount_tax, "
                + "inv.residual::bigint,\n"
                + "CASE WHEN inv.date_due IS NULL THEN 'DRAFT' "
                + "WHEN inv.\"state\" = 'paid' THEN 'LUNAS' "
                + "WHEN now() > inv.date_due THEN 'JATUH TEMPO' "
                + "ELSE concat(DATE_PART('day', inv.date_due - now())::VARCHAR, ' Hari Lagi') END as sta,\n"
                + "inv.\"state\" as ket "
                + "FROM account_invoice inv\n"
                + "INNER JOIN res_partner p ON p.\"id\" = inv.partner_id\n"
                + "INNER JOIN res_users us ON us.id = inv.user_id\n"
                + "INNER JOIN res_partner ss ON ss.id = us.partner_id\n"
                + "WHERE inv.\"state\" = 'open' AND inv.type = 'out_invoice' " + fsal + "\n"
                + "ORDER BY date_invoice";
        if (dStatus.getSelectedIndex() == 1) {
            sql = "SELECT inv.id,'false' as cek, inv.\"number\", inv.date_invoice, "
                    //+ "inv.date_due, \n"
                    + "p.\"name\", p.city, ss.name as nama_sales, "
                    //  + "inv.amount_untaxed as jml_no ,inv.amount_tax, "
                    + "inv.amount_total,\n"
                    + "CASE WHEN inv.date_due IS NULL THEN 'DRAFT' "
                    + "WHEN inv.\"state\" = 'paid' THEN 'LUNAS' "
                    + "WHEN now() > inv.date_due THEN 'JATUH TEMPO' "
                    + "ELSE concat(DATE_PART('day', inv.date_due - now())::VARCHAR, ' Hari Lagi') END as sta,\n"
                    + "inv.\"state\" as ket "
                    + "FROM account_invoice inv\n"
                    + "INNER JOIN res_partner p ON p.\"id\" = inv.partner_id\n"
                    + "INNER JOIN res_users us ON us.id = inv.user_id\n"
                    + "INNER JOIN res_partner ss ON ss.id = us.partner_id\n"
                    + "WHERE inv.\"state\" = 'paid' AND inv.type = 'out_invoice' AND inv.write_date >= '" + awal
                    + " 00:00:00' AND inv.write_date <= '" + akhir + " 23:59:59' " + fsal + "\n"
                    + "ORDER BY date_invoice";
        }

        //  System.out.println(sql);
        komp.setDataTabelCek(kon, tabelData, sql, 1, 2);
    }

    private void validasiFaktur(String aksi) {
        int hh = JOptionPane.showConfirmDialog(velidasiFaktur, "Anda yakin ini melakukan Pembayaran Piutang Penjualan ?", "Validasi Pembayaran Piutang", JOptionPane.YES_NO_OPTION);
        if (hh == JOptionPane.YES_OPTION) {
            String id_faktur = dID.getText().trim();
            String id_pelanggan = dIdCus.getText().trim();
            String no_faktur = dNoFaktur.getText().trim().replace(": ", "");
            String jml_piutang = dSisa.getText().trim();
            String cara_pembayaran = dPembayaran.getSelectedItem().toString();
            String tgl_bayar = new java.sql.Date(tgl_faktur.getDate().getTime()).toString();
            String nama_pelanggan = dNama.getText().trim().replace(": ", "");
            String nama_sales = dSales.getText().replace(": ", "");
            String jml_faktur = dTotal.getText().trim().replace(".", "");
            String jml_bayar = dBayar.getText().trim();
            String user_create = user;
            String catatan = dKeterangan.getText().trim();
            long sisa = Long.parseLong(jml_piutang);
            long bayar = Long.parseLong(jml_bayar);
            boolean cekBayar = false;
            String status = "paid";
            long sis = sisa - bayar;
            if (bayar > sisa) {
                pesan.pesanError("GAGAL PROSES PEMBAYARAN", "YANG DIBAYAR LEBIH BESAR DARI JUMLAH PIUTANG", "Mohon dicek");
            } else if (bayar < sisa) {
                int jjj = JOptionPane.showConfirmDialog(velidasiFaktur, "Anda yakin ini melakukan pembayaran lebih kecil dari jumlah piutang ?", "Bayar lebih kecil", JOptionPane.YES_NO_OPTION);
                if (jjj == JOptionPane.YES_OPTION) {
                    cekBayar = true;
                    status = "open";
                }
            } else {
                cekBayar = true;
                status = "paid";
            }

            if (cekBayar) {
                // UPDATE INVOICE
                String upd = "UPDATE account_invoice SET state = '" + status + "', residual = " + sis + " WHERE id = " + id_faktur;
                komp.setSQL(kon, upd);
                // SIMPAN PEMBAYARAN
                String[][] dataPO = {{"id_faktur", id_faktur},
                {"id_pelanggan", id_pelanggan},
                {"jml_piutang", jml_piutang},
                {"tgl_bayar", tgl_bayar},
                {"nama_pelanggan", nama_pelanggan},
                {"no_faktur", no_faktur},
                {"nama_sales", nama_sales},
                {"jml_faktur", jml_faktur},
                {"cara_pembayaran", cara_pembayaran},
                {"jml_bayar", jml_bayar},
                {"user_create", user_create},
                {"tipe", "PENJUALAN"},
                {"catatan", catatan},};
                komp.simpanDataReturn(kon, "x_pembayaran", dataPO, true);

                pesan.pesanSukses("SUKSES PROSES DATA", "Pembayaran No Faktur " + no_faktur + " Sukses disimpan");
                komp.closeDialogSukses(velidasiFaktur);
                showData();
            }
        }
    }

    private void posting() {
// SIMPAN POSTING

        String cus = dIdCus.getText();
//   String idForm = thisProcess.getFieldValue("idForm");
        String tot = dTotal.getText().replace(".", "");
        String pajak = dPPN.getText().replace(".", "");
        String sub = dDPP.getText().replace(".", "");
//   String namaSO = thisProcess.getFieldValue("namaSO");
        String invoice_date = new java.sql.Date(tgl_faktur.getDate().getTime()).toString();
        String idinv = dID.getText();
        String noFaktur = dNoFaktur.getText();
        String move_name = "INV/" + noFaktur;
        String paymen = ((Item) dPembayaran.getSelectedItem()).getValue().toString();
        String nilaiPay = komp.getStringSQL(kon, "SELECT days FROM account_payment_term_line WHERE payment_id = '" + paymen + "'");
        String[][] data = {{"create_uid", "1"},
        {"name", move_name},
        {"state", "posted"},
        {"company_id", "1"},
        {"period_id ", "4"},
        {"journal_id", "3"},
        //      {"amount", sub},
        {"ref", noFaktur},
        {"write_date", "now()"},
        {"date", invoice_date},
        {"create_date", "now()"},
        {"write_uid", "1"},
        {"partner_id", cus},};
        int id = komp.simpanDataReturn(kon, "account_move", data, false);

        if (id > 0) {
            String udpSO = "UPDATE account_invoice SET "
                    + "state = 'open', number = '" + noFaktur + "', message_last_post = now(),"
                    + "amount_untaxed = '" + sub + "', amount_tax = '" + pajak + "', "
                    + "internal_number = '" + noFaktur + "',"
                    + "residual = '" + tot + "', period_id = '4',"
                    + "amount_total = '" + tot + "', "
                    + "payment_term = '" + paymen + "',"
                    + "date_due = ('" + invoice_date + "'::date + INTERVAL '" + nilaiPay + " day')::DATE, date_invoice = '" + invoice_date + "', move_id = " + String.valueOf(id) + ", move_name = '" + move_name + "' WHERE id = " + idinv;
            komp.setSQL(kon, udpSO);

            // SIMPAN PRODUCT SALE (INCOME)
            int row = tabelFaktur.getRowCount();

            for (int bhku = 0; bhku < row; bhku++) {
                String idBarang = tabelFaktur.getValueAt(bhku, 2).toString();
                String diskon = tabelFaktur.getValueAt(bhku, 6).toString().replace(".", ",");
                String nama = tabelFaktur.getValueAt(bhku, 3).toString();
                String qty = tabelFaktur.getValueAt(bhku, 4).toString().replace(".", "");
                String total = tabelFaktur.getValueAt(bhku, 7).toString().replace(".", "");
                //   String so = tabelFaktur.getValueAt(bhku, "idSales").toString();
                //      String udpSOLine = "UPDATE account_invoice_line SET state = 'open' WHERE id = " + idBarang;
                //      komp.setSQL(kon, udpSOLine);

                String[][] data2 = {{"create_date", "now()"},
                {"journal_id", "1"},
                {"date_maturity", invoice_date},
                //           {"user_type_id", "14"},
                {"partner_id", cus},
                {"blocked", "false"},
                {"create_uid", "1"},
                {"period_id", "4"},
                //         {"amount_residual", "0"},
                {"company_id", "1"},
                //           {"credit_cash_basis", "0"},
                //            {"amount_residual_currency", "0"},
                {"debit", "0"},
                {"account_id", "17"},
                //              {"debit_cash_basis", "0"},
                //           {"reconciled", "false"},
                //               {"tax_exigible", "true"},
                //                {"balance_cash_basis", "0"},
                {"write_date", "now()"},
                {"date", invoice_date},
                {"write_uid", "1"},
                {"move_id", String.valueOf(id)},
                {"product_id", idBarang},
                //           {"company_currency_id", "3"},
                {"name", nama},
                //            {"invoice_id", idinv},
                {"credit", total},
                {"product_uom_id", "1"},
                //           {"amount_currency", "0"},
                //           {"balance", "-" + total},
                {"quantity", qty},};
                komp.simpanDataReturn(kon, "account_move_line", data2, false);
            }

// SIMPAN PAJAK
            String[][] datapajak = {{"create_date", "now()"},
            {"journal_id", "1"},
            {"date_maturity", invoice_date},
            //         {"user_type_id", "9"},
            {"partner_id", cus},
            {"blocked", "false"},
            {"create_uid", "1"},
            //           {"amount_residual", pajak},
            {"company_id", "1"},
            {"period_id", "4"},
            //          {"credit_cash_basis", "0"},
            //          {"amount_residual_currency", "0"},
            {"debit", "0"},
            {"account_id", "14"},
            //          {"debit_cash_basis", "0"},
            //          {"reconciled", "false"},
            //          {"tax_exigible", "true"},
            //           {"balance_cash_basis", "0"},
            {"write_date", "now()"},
            {"date", invoice_date},
            {"write_uid", "1"},
            {"move_id", String.valueOf(id)},
            //          {"company_currency_id", "3"},
            {"name", "PPn 10.00%"},
            //           {"invoice_id", idinv},
            {"credit", pajak},
            //           {"amount_currency", "0"},
            //           {"balance", "-" + pajak},
            {"quantity", "1"},};
            komp.simpanDataReturn(kon, "account_move_line", datapajak, false);
            // SIMPAN PIUTANG

            String[][] data3 = {{"create_date", "now()"},
            {"journal_id", "1"},
            {"date_maturity", invoice_date},
            //           {"user_type_id", "1"},
            {"partner_id", cus},
            {"blocked", "false"},
            {"create_uid", "1"},
            {"period_id", "4"},
            //           {"amount_residual", sub},
            {"company_id", "1"},
            //           {"credit_cash_basis", "0"},
            //            {"amount_residual_currency", "0"},
            {"debit", sub},
            {"account_id", "7"},
            //            {"debit_cash_basis", "0"},
            //            {"reconciled", "false"},
            //           {"tax_exigible", "true"},
            //         {"balance_cash_basis", "0"},
            {"write_date", "now()"},
            {"date", invoice_date},
            {"write_uid", "1"},
            {"move_id", String.valueOf(id)},
            //           {"company_currency_id", "3"},
            {"name", "/"},
            //            {"invoice_id", idinv},
            {"credit", "0"},
            //           {"amount_currency", "0"},
            //           {"balance", sub},
            {"quantity", "1"},};
            int yyk = komp.simpanDataReturn(kon, "account_move_line", data3, false);
            //      String inspajak = "INSERT INTO account_move_line_account_tax_rel(account_move_line_id, account_tax_id) VALUES(" + String.valueOf(yyk) + ",1)";
            //      komp.setSQL(kon, inspajak);
        }
    }

    private int cek() {
        int x = 0;
        int yy = tabelData.getRowCount();
        totRow = yy;
        for (int hh = 0; hh < yy; hh++) {
            String xy = tabelData.getValueAt(hh, 1).toString();
            if (xy.equalsIgnoreCase("true")) {
                x++;
                if (x == 1) {
                    rowCek = hh;
                } else {
                    rowCek = -1;
                }
            }
            // System.out.println(xy);
        }

        return x;
    }

    private void showValidasiFaktur(int row) {
        String id = tabelData.getValueAt(rowCek, 1).toString();
        //  String no = tabelData.getValueAt(rowCek, 3).toString();
        String nama = tabelData.getValueAt(rowCek, 6).toString();
        String sales = tabelData.getValueAt(rowCek, 7).toString();
        String nof = tabelData.getValueAt(rowCek, 3).toString();
        String sqlInv = "SELECT rp.\"name\" as nama_pelanggan, CASE WHEN rp.vat IS NULL THEN '' ELSE rp.vat END as npwp, rp.street, rp.city, a.payment_term, "
                + "CASE WHEN rp.credit_limit IS NULL OR rp.credit_limit <= 1000 THEN 99999999999::bigint ELSE rp.credit_limit::bigint END as limit, "
                + "rp.id, a.amount_untaxed::bigint as jml, residual::bigint "
                + "FROM account_invoice a INNER JOIN res_partner rp ON rp.\"id\" = a.partner_id WHERE a.\"id\" = '" + id + "' ";
        Object[] data = komp.setDataEdit(kon, sqlInv);
        String npwp = data[1].toString();
        String alamat = data[2].toString();
        String kota = data[3].toString();
        String tempo = data[4].toString();
        String limit = data[5].toString();
        String idPihak = data[6].toString();
        String jml = data[7].toString();
        String sisa = data[8].toString();
        //    String piutang = komp.getStringSQL(kon, "SELECT sum(residual)::bigint as jml FROM account_invoice WHERE partner_id = '" + id + "'");
        //      if (piutang.length() == 0) {
        //         piutang = "0";
        //     }
        //    long total = Long.parseLong(piutang) + Long.parseLong(jml);
//        long lim = Long.parseLong(limit);
        // System.out.println(limit);
//        if (total > lim) {
//            dStatusPiutang.setText("TIDA BISA HUTANG");
//            btnValid.setEnabled(false);
//            btnValidPrint.setEnabled(false);
//        } else {
//        dStatusPiutang.setText("");
        btnValid.setEnabled(true);
        dBayar.setText(sisa);
        dSisa.setText(sisa);
//        btnValidPrint.setEnabled(true);
        //       }
//       dPembayaran.setSelectedIndex(Integer.parseInt(tempo));
        dNama.setText(": " + nama);
        dNPWP.setText(": " + npwp);
        dAlamat.setText(": " + alamat);
        dKota.setText(": " + kota);
        dSales.setText(": " + sales);
        dNoFaktur.setText(": " + nof);
        //      dLimit.setText(komp.ribuan(limit));
        //      dPiutang.setText(komp.ribuan(piutang));
        dIdCus.setText(idPihak);
        dID.setText(id);
        dID.setVisible(false);
        dIdCus.setVisible(false);
//        if (npwp.length() <= 8) {
//            dTipe.setText("TIDAK PAJAK");
//            dNoFaktur.setText("NPSA-YYDD-XXXX");
//        } else {
//            dTipe.setText("PAJAK");
//            dNoFaktur.setText("NPS-YYDD-XXXX");
//        }
        String sqlDetail = "SELECT v.id,p.id as id_barang, p.name_template, v.quantity::bigint, v.price_unit::bigint, discount::bigint as dis, "
                + "(v.quantity * (v.price_unit - (v.price_unit * (v.discount/100))))::bigint as tot "
                + "FROM account_invoice_line v "
                + "INNER JOIN product_product p ON p.\"id\" = v.product_id\n"
                + "INNER JOIN product_template pt ON pt.\"id\" = p.product_tmpl_id\n"
                + "WHERE v.invoice_id = '" + id + "'";

        komp.setDataTabel(kon, tabelFaktur, sqlDetail, 1);

        hitungHarga();
    }

    private void cetakFakturPenjualan(boolean ok) {
        int yy = tabelData.getRowCount();
        JasperPrint page1 = null;
        if (ok) {
            for (int hh = 0; hh < yy; hh++) {
                String id = tabelData.getValueAt(hh, 1).toString();
                String sal = tabelData.getValueAt(hh, 8).toString();
                if (hh == 0) {
                    Hashtable hparam = new Hashtable(1);
                    hparam.put("id", id);
                    hparam.put("user", user);
                    hparam.put("sales", sal);
                    page1 = jr.getReport("invoice_with_pajak", hparam, kon, false);
                } else {
                    Hashtable hparam = new Hashtable(1);
                    hparam.put("id", id);
                    hparam.put("user", user);
                    hparam.put("sales", sal);
                    JasperPrint page2 = jr.getReport("invoice_with_pajak", hparam, kon, false);
                    page1 = jr.multipageLinking(page1, page2);
                }
            }

        } else {
            int uu = 0;
            for (int hh = 0; hh < yy; hh++) {
                String id = tabelData.getValueAt(hh, 1).toString();
                String cek = tabelData.getValueAt(hh, 2).toString();
                String sal = tabelData.getValueAt(hh, 8).toString();
                if (cek.equalsIgnoreCase("true")) {
                    if (uu == 0) {
                        Hashtable hparam = new Hashtable(1);
                        hparam.put("id", id);
                        hparam.put("user", user);
                        hparam.put("sales", sal);
                        page1 = jr.getReport("invoice_with_pajak", hparam, kon, false);
                    } else {
                        Hashtable hparam = new Hashtable(1);
                        hparam.put("id", id);
                        hparam.put("user", user);
                        hparam.put("sales", sal);
                        JasperPrint page2 = jr.getReport("invoice_with_pajak", hparam, kon, false);
                        page1 = jr.multipageLinking(page1, page2);
                    }
                    uu++;
                }
            }
        }
        jr.tampilReport(page1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuFaktur = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        menuSurat = new javax.swing.JPopupMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        velidasiFaktur = new javax.swing.JDialog();
        jPanel12 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelFaktur = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        dSales = new javax.swing.JLabel();
        dNama = new javax.swing.JLabel();
        dNPWP = new javax.swing.JLabel();
        dTempo = new javax.swing.JLabel();
        dAlamat = new javax.swing.JLabel();
        dKota = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        dNoFaktur = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        dPembayaran = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        dTempo2 = new javax.swing.JLabel();
        tgl_faktur = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        dTempo1 = new javax.swing.JLabel();
        btnValid = new javax.swing.JButton();
        dID = new javax.swing.JLabel();
        dIdCus = new javax.swing.JLabel();
        dKeterangan = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        dTempo3 = new javax.swing.JLabel();
        dBayar = new javax.swing.JTextField();
        dTempo4 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        dSisa = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        label1 = new javax.swing.JLabel();
        dTotal = new javax.swing.JLabel();
        label2 = new javax.swing.JLabel();
        dPPN = new javax.swing.JLabel();
        label3 = new javax.swing.JLabel();
        dDPP = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelData = new javax.swing.JTable();
        tabelData = new tabelStatus(((DefaultTableModel) tabelData.getModel()));
        jPanel4 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnValidasi = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        labelTgl = new javax.swing.JLabel();
        tgl_mulai = new com.toedter.calendar.JDateChooser();
        labelFak = new javax.swing.JLabel();
        tgl_sampai = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        sales = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        textFilter = new javax.swing.JTextField();
        dStatus = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/category.png"))); // NOI18N
        jMenuItem1.setText("Cetak Terpilih");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        menuFaktur.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        jMenuItem2.setText("Cetak Semua");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuFaktur.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/category.png"))); // NOI18N
        jMenuItem3.setText("Cetak Terpilih");
        menuSurat.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        jMenuItem4.setText("Cetak Semua");
        menuSurat.add(jMenuItem4);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.BorderLayout());

        jPanel17.setOpaque(false);
        jPanel17.setLayout(new java.awt.GridLayout(1, 0));

        tabelFaktur.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tabelFaktur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "Kode", "Nama Barang", "Qty", "Harga", "Diskon (%)", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelFaktur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelFakturKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tabelFaktur);

        jPanel17.add(jScrollPane2);

        jPanel12.add(jPanel17, java.awt.BorderLayout.CENTER);

        jPanel18.setOpaque(false);
        jPanel18.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel18.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 0, 30, 200));

        jLabel10.setText("Nama Pelanggan");
        jPanel18.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 110, 20));

        jLabel11.setText("NPWP");
        jPanel18.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 130, 20));

        jLabel13.setText("Salesman");
        jPanel18.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 70, -1));

        jLabel15.setText("Kota");
        jPanel18.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 80, -1));

        dSales.setText(": DATA");
        jPanel18.add(dSales, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 250, -1));

        dNama.setText(": DATA");
        jPanel18.add(dNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 250, -1));

        dNPWP.setText(": DATA");
        jPanel18.add(dNPWP, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 40, 250, -1));

        dTempo.setText(":");
        jPanel18.add(dTempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 130, -1, 20));

        dAlamat.setText(": DATA");
        jPanel18.add(dAlamat, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, 250, -1));

        dKota.setText(": DATA");
        jPanel18.add(dKota, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, 250, -1));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("NO FAKTUR ");
        jPanel18.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 90, 30));

        dNoFaktur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dNoFaktur.setText(": XXXXXX");
        jPanel18.add(dNoFaktur, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 220, 30));

        jLabel16.setText("Alamat");
        jPanel18.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 80, -1));

        dPembayaran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TRANSFER BANK", "TUNAI", "GIRO", "BANK GARANSI/CEK" }));
        jPanel18.add(dPembayaran, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 40, 230, -1));

        jLabel14.setText("Tanggal Bayar");
        jPanel18.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 130, 20));

        jLabel17.setText("Cara Pembayaran");
        jPanel18.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 130, 20));

        dTempo2.setText(":");
        jPanel18.add(dTempo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, -1, 20));

        tgl_faktur.setDate(new java.util.Date());
        tgl_faktur.setDateFormatString("dd/MM/yyyy");
        tgl_faktur.setPreferredSize(new java.awt.Dimension(110, 22));
        jPanel18.add(tgl_faktur, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 10, -1, -1));

        jLabel2.setText("Catatan");
        jPanel18.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 110, -1));

        dTempo1.setText(":");
        jPanel18.add(dTempo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 40, 10, 20));

        btnValid.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnValid.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/ok.png"))); // NOI18N
        btnValid.setText("Konfirmasi Pembayaran");
        btnValid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidActionPerformed(evt);
            }
        });
        jPanel18.add(btnValid, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 160, 200, -1));

        dID.setText("Alamat");
        jPanel18.add(dID, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 160, 60, -1));

        dIdCus.setText("jLabel9");
        jPanel18.add(dIdCus, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 160, -1, -1));
        jPanel18.add(dKeterangan, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 130, 300, -1));

        jLabel9.setText("Bayar");
        jPanel18.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, 110, -1));

        dTempo3.setText(":");
        jPanel18.add(dTempo3, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 100, -1, 20));

        dBayar.setDocument(new saring_karakter().setAllOnlyDigit());
        jPanel18.add(dBayar, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 100, 130, -1));

        dTempo4.setText(":");
        jPanel18.add(dTempo4, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, -1, 20));

        jLabel12.setText("Total Piutang");
        jPanel18.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, 110, -1));

        dSisa.setEditable(false);
        jPanel18.add(dSisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 70, 130, -1));

        jPanel12.add(jPanel18, java.awt.BorderLayout.PAGE_START);

        jPanel19.setOpaque(false);
        jPanel19.setPreferredSize(new java.awt.Dimension(20, 100));
        jPanel19.setLayout(new java.awt.GridLayout(1, 0));

        jPanel20.setOpaque(false);
        jPanel20.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 10));
        jPanel19.add(jPanel20);

        jPanel21.setOpaque(false);
        jPanel21.setLayout(new java.awt.GridLayout(3, 1));

        label1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label1.setText("TOTAL");
        label1.setPreferredSize(new java.awt.Dimension(40, 29));
        jPanel21.add(label1);

        dTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dTotal.setText("XXXXX");
        jPanel21.add(dTotal);

        label2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label2.setText("PPN");
        jPanel21.add(label2);

        dPPN.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dPPN.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dPPN.setText("XXXXX");
        jPanel21.add(dPPN);

        label3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label3.setText("DPP");
        jPanel21.add(label3);

        dDPP.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dDPP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dDPP.setText("XXXXXX");
        jPanel21.add(dDPP);

        jPanel19.add(jPanel21);

        jPanel12.add(jPanel19, java.awt.BorderLayout.PAGE_END);

        velidasiFaktur.getContentPane().add(jPanel12, java.awt.BorderLayout.CENTER);

        jPanel13.setBackground(new java.awt.Color(51, 255, 204));
        jPanel13.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        jLabel7.setBackground(new java.awt.Color(255, 255, 204));
        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("PEMBAYARAN FAKTUR PENJUALAN");
        jPanel13.add(jLabel7);

        velidasiFaktur.getContentPane().add(jPanel13, java.awt.BorderLayout.PAGE_START);
        velidasiFaktur.getContentPane().add(jPanel14, java.awt.BorderLayout.PAGE_END);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setPreferredSize(new java.awt.Dimension(20, 10));
        velidasiFaktur.getContentPane().add(jPanel15, java.awt.BorderLayout.LINE_END);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setPreferredSize(new java.awt.Dimension(20, 10));
        velidasiFaktur.getContentPane().add(jPanel16, java.awt.BorderLayout.LINE_START);

        setTitle("Faktur Penjualan");

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        tabelData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No", "ID", "cek", "No. Faktur", "Tanggal", "Nama Pelanggan", "Kota", "Sales", "Piutang", "Keterangan", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDataMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelData);

        jPanel3.add(jScrollPane1);

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 5));

        btnValidasi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/validasi.gif"))); // NOI18N
        btnValidasi.setMargin(new java.awt.Insets(1, 1, 1, 1));
        btnValidasi.setOpaque(false);
        btnValidasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidasiActionPerformed(evt);
            }
        });
        jPanel9.add(btnValidasi);

        jPanel4.add(jPanel9);

        jPanel10.setOpaque(false);
        jPanel10.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 25, 25));
        jPanel4.add(jPanel10);

        jPanel1.add(jPanel4, java.awt.BorderLayout.PAGE_START);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel5, java.awt.BorderLayout.PAGE_END);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel6, java.awt.BorderLayout.LINE_END);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jPanel7, java.awt.BorderLayout.LINE_START);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel8.setLayout(new java.awt.GridLayout(1, 2));

        jPanel2.setBackground(new java.awt.Color(51, 255, 204));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 10, 100));

        labelTgl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTgl.setText("s/d");
        jPanel2.add(labelTgl, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 30, 20));

        tgl_mulai.setDate(new java.util.Date());
        tgl_mulai.setDateFormatString("dd/MM/yyyy");
        tgl_mulai.setPreferredSize(new java.awt.Dimension(110, 22));
        jPanel2.add(tgl_mulai, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, -1, -1));

        labelFak.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelFak.setText("Tgl Faktur :");
        jPanel2.add(labelFak, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 90, 20));

        tgl_sampai.setDate(new java.util.Date());
        tgl_sampai.setDateFormatString("dd/MM/yyyy");
        tgl_sampai.setPreferredSize(new java.awt.Dimension(110, 22));
        jPanel2.add(tgl_sampai, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 10, -1, -1));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Filter :");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 20));

        sales.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(sales, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 280, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/cari.png"))); // NOI18N
        jButton1.setText("Tampil Data");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, -1, -1));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Nama Sales :");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 90, 20));
        jPanel2.add(textFilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 170, -1));

        dStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "BELUM LUNAS", "LUNAS" }));
        dStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dStatusActionPerformed(evt);
            }
        });
        jPanel2.add(dStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 120, -1));

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Status :");
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 80, 20));

        jPanel8.add(jPanel2);

        jPanel11.setLayout(new java.awt.GridLayout(1, 0, 20, 20));

        jLabel6.setBackground(new java.awt.Color(51, 255, 204));
        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("TAGIHAN PENJUALAN     ");
        jLabel6.setOpaque(true);
        jPanel11.add(jLabel6);

        jPanel8.add(jPanel11);

        getContentPane().add(jPanel8, java.awt.BorderLayout.PAGE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        showData();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        cetakFakturPenjualan(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        cetakFakturPenjualan(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btnValidasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidasiActionPerformed
        int yy = tabelData.getRowCount();
        rowCek = tabelData.getSelectedRow();
        totRow = yy;
        if (rowCek >= 0) {

//            jml.setText(String.valueOf(rowCek + 1));
//            total.setText(String.valueOf(totRow));
            showValidasiFaktur(rowCek);

            komp.showDialog(velidasiFaktur, 1000, 600);
        }
    }//GEN-LAST:event_btnValidasiActionPerformed

    private void tabelDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDataMouseClicked

        if (evt.getClickCount() >= 2) {

            int yy = tabelData.getRowCount();
            rowCek = tabelData.getSelectedRow();
            totRow = yy;
            if (rowCek >= 0) {

                String byr = tabelData.getValueAt(rowCek, 10).toString();
                if (byr.equalsIgnoreCase("open")) {
//                    jml.setText(String.valueOf(rowCek + 1));
//                    total.setText(String.valueOf(totRow));
                    showValidasiFaktur(rowCek);

                    komp.showDialog(velidasiFaktur, 1000, 600);
                }
            }

        }
    }//GEN-LAST:event_tabelDataMouseClicked

    private void tabelFakturKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelFakturKeyReleased

    }//GEN-LAST:event_tabelFakturKeyReleased

    private void btnValidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidActionPerformed
        validasiFaktur("NO");
    }//GEN-LAST:event_btnValidActionPerformed

    private void dStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dStatusActionPerformed

        if (dStatus.getSelectedIndex() == 0) {
            tgl_mulai.setVisible(false);
            tgl_sampai.setVisible(false);
            labelTgl.setVisible(false);
            btnValidasi.setVisible(true);
            labelFak.setVisible(false);
            showData();
        } else {
            tgl_mulai.setVisible(true);
            tgl_sampai.setVisible(true);
            labelTgl.setVisible(true);
            btnValidasi.setVisible(false);
            labelFak.setVisible(true);
            showData();
        }
    }//GEN-LAST:event_dStatusActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnValid;
    private javax.swing.JButton btnValidasi;
    private javax.swing.JLabel dAlamat;
    private javax.swing.JTextField dBayar;
    private javax.swing.JLabel dDPP;
    private javax.swing.JLabel dID;
    private javax.swing.JLabel dIdCus;
    private javax.swing.JTextField dKeterangan;
    private javax.swing.JLabel dKota;
    private javax.swing.JLabel dNPWP;
    private javax.swing.JLabel dNama;
    private javax.swing.JLabel dNoFaktur;
    private javax.swing.JLabel dPPN;
    private javax.swing.JComboBox<String> dPembayaran;
    private javax.swing.JLabel dSales;
    private javax.swing.JTextField dSisa;
    private javax.swing.JComboBox<String> dStatus;
    private javax.swing.JLabel dTempo;
    private javax.swing.JLabel dTempo1;
    private javax.swing.JLabel dTempo2;
    private javax.swing.JLabel dTempo3;
    private javax.swing.JLabel dTempo4;
    private javax.swing.JLabel dTotal;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JLabel label3;
    private javax.swing.JLabel labelFak;
    private javax.swing.JLabel labelTgl;
    private javax.swing.JPopupMenu menuFaktur;
    private javax.swing.JPopupMenu menuSurat;
    private javax.swing.JComboBox<String> sales;
    private javax.swing.JTable tabelData;
    private javax.swing.JTable tabelFaktur;
    private javax.swing.JTextField textFilter;
    private com.toedter.calendar.JDateChooser tgl_faktur;
    private com.toedter.calendar.JDateChooser tgl_mulai;
    private com.toedter.calendar.JDateChooser tgl_sampai;
    private javax.swing.JDialog velidasiFaktur;
    // End of variables declaration//GEN-END:variables

    class tabelStatus extends JTable implements Serializable {

        private int rolloverRowIndex = -1;

        public tabelStatus(DefaultTableModel tabTabel) {
            setModel(tabTabel);
            this.setSelectionBackground(Color.magenta);
            this.setSelectionForeground(Color.BLACK);

        }

        int x = 0;

        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            Component c = super.prepareRenderer(renderer, row, column);
            String s = super.getModel().getValueAt(row, 10).toString();
            //    int bb = Integer.valueOf(s);
            //    String cc = "";
            //    String status = "Gagal";

            c.setBackground(Color.WHITE);
            if (s.equalsIgnoreCase("draft")) {
                c.setBackground(Color.WHITE);
                //  c.setFont(new Font("Tahoma", 0, 11));
            } else if (s.equalsIgnoreCase("open")) {
                c.setBackground(Color.WHITE);
                //  c.setFont(new Font("Tahoma", 0, 11));
            } else if (s.equalsIgnoreCase("paid")) {
                c.setBackground(Color.green);
                //  c.setFont(new Font("Tahoma", 0, 11));
            }

            return c;
        }

    }
}
